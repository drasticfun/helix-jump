﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {

    [SerializeField] Transform startPlatform;
    [SerializeField] Transform finishPlatform;
    [Space]
    [SerializeField] GameObject pillar;
    //[SerializeField] GameObject[] platforms;

    #region Parameter variables
    public int numberOfPlatforms = 0;
    [SerializeField] int numberOfActivePlatforms = 2;
    [SerializeField] int platformSpawnMultiplier = 0;
    [SerializeField] float rangeBetweenPlatform = 0;
    [HideInInspector] public int platformDifficult = 0;

    private float tempRange = 0f;
    private int platformCount = 0;
    #endregion

    private MeshRenderer _renderer;
    private List<GameObject> pillarsList = new List<GameObject>();

    // Use this for initialization
    private void OnEnable()
    {
        GameManager.OnPauseGame += ClearData;
        GameManager.OnRestartLevel += SpawnPillar;
        GameManager.OnRestartLevel += SpawnObjectFromPool;
        
        Ball.OnDetonate += SpawnObjectFromPool;
        Ball.OnFinish += IncreaseDifficult;

    }

    private void OnDisable()
    {
        GameManager.OnPauseGame -= ClearData;
        GameManager.OnRestartLevel -= SpawnPillar;
        GameManager.OnRestartLevel -= SpawnObjectFromPool;

        Ball.OnDetonate -= SpawnObjectFromPool;
        Ball.OnFinish -= IncreaseDifficult;
    }
    
    void Start () {

        _renderer = pillar.GetComponent<MeshRenderer>();
        startPlatform.rotation = Quaternion.Euler(new Vector3(0, -50f, 0));

        SpawnPillar(false);
        SpawnObjectFromPool(false);
    }

    /// <summary>
    /// Increase level difficult
    /// </summary>
    /// <param name="state"></param>
    void IncreaseDifficult(bool state)
    {
        numberOfPlatforms += platformSpawnMultiplier;

        if (platformDifficult < ObjectsPool.Instance.prefab.Length)
            platformDifficult++;
        
    }
    void ClearData()
    {
        tempRange = 0f;
        platformCount = 0;
    }

    /// <summary>
    /// Method that generates Pillar with start and end platforms
    /// </summary>
    /// <param name="state"></param>
    void SpawnPillar(bool state)
    {
        float pillarLength = rangeBetweenPlatform * (numberOfPlatforms + 1f);
        pillar.transform.localScale = new Vector3(pillar.transform.localScale.x, pillarLength / 2, pillar.transform.localScale.z);
        pillar.transform.parent.rotation = Quaternion.Euler(Vector3.zero);
        finishPlatform.position = new Vector3(pillar.transform.position.x, _renderer.bounds.min.y, pillar.transform.position.z);
        startPlatform.gameObject.SetActive(true);
        startPlatform.position = new Vector3(pillar.transform.position.x, _renderer.bounds.max.y - 2f*rangeBetweenPlatform, pillar.transform.position.z);
    }

    /// <summary>
    /// Spawn platforms method
    /// </summary>
    /// <param name="state"></param>
    void SpawnObjectFromPool(bool state)
    {

        float randomRotation = 0;
        
        if (!state)
        {
            
            tempRange = 3f *rangeBetweenPlatform;
            for (int i = 0; i < numberOfActivePlatforms; i++)
            {
                randomRotation = Random.Range(90f, 270f);
                var platform = ObjectsPool.Instance.GetFromPool(numberOfPlatforms, platformDifficult);
                platform.transform.position = new Vector3(pillar.transform.position.x, _renderer.bounds.max.y - tempRange, pillar.transform.position.z);
                platform.transform.localRotation = Quaternion.Euler(platform.transform.rotation.x, randomRotation, platform.transform.rotation.z);
                tempRange += rangeBetweenPlatform;
            }
        }
        else if (platformCount < numberOfPlatforms - numberOfActivePlatforms - 1)
        {
            randomRotation = Random.Range(90f, 270f);
            var platform = ObjectsPool.Instance.GetFromPool(numberOfPlatforms, platformDifficult);
            platform.transform.position = new Vector3(pillar.transform.position.x, _renderer.bounds.max.y - tempRange, pillar.transform.position.z);
            platform.transform.localRotation = Quaternion.Euler(platform.transform.rotation.x, randomRotation, platform.transform.rotation.z);
            tempRange += rangeBetweenPlatform;
        }

        platformCount++;
        
    }
}
