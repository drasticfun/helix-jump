﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformControl : MonoBehaviour {

    [SerializeField] GameObject pillar;
    [SerializeField] float sensitive;
   
	
	// Update is called once per frame
	void LateUpdate () {
        TouchControl();
	}

    void TouchControl()
    {   
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            if(GameManager.instance.Revived)
                GameManager.instance.ResumeGame();
            
            Vector2 moving = Input.GetTouch(0).deltaPosition;
            //moving.Normalize();
            //camera.transform.RotateAround(Object.position, Vector3.forward, moving.x);
            pillar.transform.eulerAngles += new Vector3(0f, -moving.x * sensitive, 0f);
        }
    }
}
