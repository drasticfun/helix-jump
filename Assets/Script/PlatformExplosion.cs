﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformExplosion : MonoBehaviour {


    public GameObject bomb;
    #region Parameters variables
    [SerializeField] float power = 10f;
    [SerializeField] float radius = 5f;
    [SerializeField] float upForce = 1f;
    #endregion
    // Use this for initialization

    private void OnEnable()
    {
        Ball.OnDetonate += Detonate;
        GameManager.OnPauseGame += ResetCoroutine;
    }
    private void OnDisable()
    {
        Ball.OnDetonate -= Detonate;
        GameManager.OnPauseGame -= ResetCoroutine;
    }

    /// <summary>
    /// Explosion logic method
    /// </summary>
    /// <param name="state"></param>
    void Detonate(bool state)
    {
        Vector3 explosionPosition = bomb.transform.position;
        Collider[] colliders = Physics.OverlapBox(explosionPosition, new Vector3(2f, 0.3f, 2f));
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponentInParent<Rigidbody>();
            if (rb != null && hit.gameObject.tag != "Player")
            {
                rb.useGravity = true;
                rb.isKinematic = false;
                hit.enabled = false;
                rb.AddExplosionForce(power, rb.transform.parent.position, 1f, upForce, ForceMode.Acceleration);
                StartCoroutine(DisablePlatform(rb, hit, state));
                state = false;
            } else if (hit.gameObject.tag == "Platform" || hit.gameObject.tag == "StartPlatform")
            {
                hit.enabled = false;
            }
        }
        
    }
    void ResetCoroutine()
    {
        StopAllCoroutines();
    }
    /// <summary>
    /// IEnumerator that disabling platform after detonate
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    IEnumerator DisablePlatform(Rigidbody _rigidbody, Collider hit, bool state)
    {

        yield return new WaitForSeconds(1f);
        if (_rigidbody != null )
        {
            _rigidbody.useGravity = false;
            _rigidbody.isKinematic = true;
            hit.enabled = true;

            if (state && _rigidbody.transform.parent.tag == "Platform")
                ObjectsPool.Instance.AddToPool(_rigidbody.transform.parent.gameObject);
            else if (_rigidbody != null && _rigidbody.transform.parent.tag == "StartPlatform")
                _rigidbody.transform.parent.gameObject.SetActive(false);
        }
        
    }
}
