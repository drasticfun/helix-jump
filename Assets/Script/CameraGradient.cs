﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class CameraGradient : MonoBehaviour {

    public RawImage img;
    private Texture2D backgroundTexture;
    public Gradient gradient;

    void Awake()
    {
        backgroundTexture = new Texture2D(1, 4);
        backgroundTexture.wrapMode = TextureWrapMode.Clamp;
        backgroundTexture.filterMode = FilterMode.Bilinear;

        
        SetColor(gradient.colorKeys[0].color, gradient.colorKeys[1].color, gradient.colorKeys[2].color, gradient.colorKeys[3].color);
    }

    public void SetColor(Color color1, Color color2, Color color3, Color color4)
    {
        backgroundTexture.SetPixels(new Color[] { color1, color2, color3, color4});
        backgroundTexture.anisoLevel = 4;
        backgroundTexture.Apply();
        img.texture = backgroundTexture;
    }
}
