﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Helix;
using Snowglobe.Game;
using Snowglobe.Services;
using UnityEngine;

public class Ball : MonoBehaviour {

    Rigidbody rb;
    
    GameManager gameManager;
    List<Collider> list = new List<Collider>();

    [SerializeField] AudioClip[] clips;
    [SerializeField] Transform spawnPoint;
    [SerializeField] ParticleSystem fireParticles;
    [SerializeField] ParticleSystem fireStartParticle;
    [SerializeField] ParticleSystem splashParticles;
    [SerializeField] GameObject splatterEffect;

    //public static event Action showMessage = delegate { };
    public static event Action<bool> OnDetonate = delegate { };
    public static event Action<bool> OnPlatformPassed = delegate { };
    public static event Action<bool> OnFinish = delegate { };
    public static event Action OnDie = delegate { };

    int platformCount = 0; // counting passed platforms
    bool isDead = false;

    // DrasticFun Changes
    [SerializeField] private HelixGame _game;
    private Sound _sound;
    private float _pitch = 1f;
    
    private void OnEnable()
    {
        GameManager.OnRestartLevel += RespawnBall;
    }

    private void OnDisable()
    {
        GameManager.OnRestartLevel -= RespawnBall;
    }
    // Use this for initialization
    void Start ()
    {
        _sound = _game.getService<Sound>();
        rb = GetComponent<Rigidbody>();
        RespawnBall(true);
	}

    private void FixedUpdate()
    {

        if (rb.velocity.y < -7f)
        {
            rb.velocity = new Vector3(0, -7f, 0); // ball velocity constraint
        }
        
    }

    /// <summary>
    /// Respawn ball position to spawn point
    /// </summary>
    void RespawnBall(bool state)
    {
        transform.position = new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y + 0.7f, spawnPoint.transform.position.z + 0.7f);
    }

    /// <summary>
    /// IEnumerator that spawn splatter decals on surface
    /// </summary>
    /// <param name="collision"></param>
    /// <returns></returns>
    IEnumerator SplatterEffect(Collision collision)
    {
        GameObject go = Instantiate(splatterEffect, collision.contacts[0].point + new Vector3(0, 0.03f, 0), Quaternion.identity) as GameObject;
        
        go.transform.SetParent(collision.transform);

        yield return new WaitForSeconds(5f);
        Destroy(go);
    }

    /// <summary>
    /// Check raycast hit collider
    /// </summary>
    /// <param name="collider"></param>
    void ObjectHit(Collider collider)
    {
        
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 1f))
        {
            isDead = false;
            if (hit.collider.tag == "DeadZone" || hit.collider.isTrigger && collider.tag == "DeadZone" || collider.tag == "DeadWall") 
            {
                isDead = true;
            } 
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        rb.velocity = new Vector3(0, 4.5f, 0);
        _pitch = 1f;
        ObjectHit(collision.collider);

        if (isDead)
            list.Add(collision.collider);
        // if counting platform passed equal or more than 2 Detonate platform;
        if (platformCount > 2)
        {
            OnDetonate(true);
        }
        else if (isDead && list.Count < 2 && GameManager.instance.resumeState == false && platformCount <= 2) // if ball collision with platform with tag Dead Zone starts event OnDie
        {
            _sound.play(clips[2], _pitch);
            //audio.clip = clips[2];
            OnDie();
            list.Clear();
        }
        else if(collision.collider.tag == "Finish") // if ball collision with platform with tag Finish starts event OnFinish
        {
            OnFinish(true);
            //GameManager.instance.OnPauseMessage("Level done!", "Next level");
        } 

        
        StartCoroutine(SplatterEffect(collision));
        splashParticles.Play();                     // play splash effect particles 
        _sound.play(clips[0], _pitch);
        //audio.Play();

        if (fireStartParticle.isPlaying)
            fireStartParticle.Stop();
        if (fireParticles.isPlaying)
            fireParticles.Stop();                       // stop fire effect particles

        CameraFocus.startFollow = false;

        if (list.Count >= 2)
            list.Clear();

        GameManager.instance.resumeState = false;
        platformCount = 0;                                  // zeroing count

    }
    private void OnTriggerEnter(Collider other)
    {
        CameraFocus.startFollow = true;
        //audio.clip = clips[1];

        if (other.tag == "Platform" || other.tag == "StartPlatform")                // if ball passed through platform
        {
            platformCount += 1;
            _sound.play(clips[1], _pitch);
            OnPlatformPassed(true);
            OnDetonate(true);
        }
        if (CameraFocus.startFollow == true && platformCount > 1)  // play fall effect particles
        {
            _pitch += 0.1f;
            fireStartParticle.Play();
        }
        if (CameraFocus.startFollow == true && platformCount > 2) // play fire effect particles
        {
            fireParticles.Play();
            fireStartParticle.Stop();
        }
        GameManager.instance.comboCount = platformCount;
    }

    public void UnFreeze()
    {
        rb.isKinematic = false;
    }

    public void Freeze()
    {
        rb.isKinematic = true;
    }
}

