﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {

    [SerializeField] Transform[] partsTransform;
    GameObject[] splatters;
    private void OnEnable()
    {
        GameManager.OnPauseGame += SendObjectToPool;
        ResetPlatformParams();

    }
    private void OnDisable()
    {
        GameManager.OnPauseGame -= SendObjectToPool;
    }
    void ResetPlatformParams()
    {
        splatters = GameObject.FindGameObjectsWithTag("SplatterDecal");
        foreach(GameObject go in splatters)
        {
            Destroy(go);
        }
        if (transform.GetComponentInChildren<BoxCollider>())
        {
            transform.GetComponentInChildren<BoxCollider>().enabled = true;
        }
        foreach (Transform _transform in partsTransform)
        {
            _transform.localPosition = Vector3.zero;
            _transform.localRotation = Quaternion.Euler(Vector3.zero);

            _transform.GetComponent<Rigidbody>().useGravity = false;
            _transform.GetComponent<Rigidbody>().isKinematic = true;

            Collider[] colliders;

            colliders = _transform.GetComponentsInChildren<Collider>();
            
            foreach (Collider hit in colliders)
            {
                hit.enabled = true;
            }
        }
    }
    void SendObjectToPool()
    {
        if (gameObject.tag == "StartPlatform")
        {
            gameObject.SetActive(false);
        }
        else
        {
            ObjectsPool.Instance.AddToPool(gameObject);
        }
    }


}
