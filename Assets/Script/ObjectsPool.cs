﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool : MonoBehaviour {

    [SerializeField] GameObject poolParent;
    [Space]
    public GameObject[] prefab;

    //public int numberOfPlatforms = 0;
    //[HideInInspector]
    //public int platformDifficult = 0;
    private int currentDifficult = 0;
    private int currentNumberOfPlatforms = 0;
    private Queue<GameObject> availableObjects = new Queue<GameObject>();

    public static ObjectsPool Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public GameObject GetFromPool(int numberOfPlatforms, int platformDifficult)
    {
        
        if (availableObjects.Count == 0)
        {
            GrowPool(numberOfPlatforms, platformDifficult);
        } 
        else if (numberOfPlatforms > currentNumberOfPlatforms)
        {
            GrowPool(platformDifficult);
            currentNumberOfPlatforms = numberOfPlatforms;
        }
        var instance = availableObjects.Dequeue();
       
        instance.SetActive(true);
        return instance;
    }

    private void GrowPool(int numberOfPlatforms, int platformDifficult)
    {
        int randomNum = 0;
        for (int i = 0; i < numberOfPlatforms; i++)
        {
            randomNum = Random.Range(0, platformDifficult);
            var instanceToAdd = Instantiate(prefab[randomNum]);
            instanceToAdd.transform.SetParent(poolParent.transform);
            AddToPool(instanceToAdd);
        }
    }

    private void GrowPool(int platformDifficult)
    {
        int randomNum = 0;
        randomNum = Random.Range(5, platformDifficult);
        var instanceToAdd = Instantiate(prefab[randomNum]);
        instanceToAdd.transform.SetParent(poolParent.transform);
        AddToPool(instanceToAdd);
    }

    public void AddToPool(GameObject instance)
    {
        instance.SetActive(false);
        availableObjects.Enqueue(instance);
    }
}
