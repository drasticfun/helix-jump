﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour {

    public static bool startFollow = false; // camera follow state
    public float bias = 2;  // camera bias 

    [SerializeField]
    private Transform ball;

    private void OnEnable()
    {
        GameManager.OnRestartLevel += Follow; 
    }

    private void OnDisable()
    {
        GameManager.OnRestartLevel -= Follow;
    }

    private void Start()
    {
        startFollow = true;
        Follow(startFollow);
    }

    private void FixedUpdate()
    {
        Follow(startFollow);
    }

    /// <summary>
    /// Camera follow method
    /// </summary>
    void Follow(bool state)
    {
        if(state)
        {
            transform.position = new Vector3(transform.position.x, ball.position.y + bias, transform.position.z);
        }
    }

}
