﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MoveAnimation : MonoBehaviour {

    [SerializeField] float endValue = 0f;
	// Use this for initialization
	void Start () {
        TweenParams tParams = new TweenParams().SetLoops(-1, LoopType.Yoyo);
        transform.DOLocalRotate(new Vector3(0, endValue, 0), 1.5f).SetAs(tParams);
    }

}
