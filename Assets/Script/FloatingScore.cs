﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingScore : MonoBehaviour {

    Text text;
    [SerializeField] float fadeDuration = 2.0f;
    [SerializeField] float speed = 2.0f;
    int comboScore = 0;
	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        text.text = "+" + (GameManager.instance.levelNumber * GameManager.instance.comboCount).ToString();
        StartCoroutine(Fade());
	}

    IEnumerator Fade ()
    {
        float fadeSpeed = (float)1.0f / fadeDuration;
        Color c = text.color;

        for (float t = 0.0f; t < 1.0f; t+= Time.deltaTime * fadeSpeed)
        {
            c.a = Mathf.Lerp(1, 0, t);
            text.color = c;
            yield return true;
        }

        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void LateUpdate () {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
	}
}
