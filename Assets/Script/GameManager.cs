﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Snowglobe.Core;
using Snowglobe.Game;
using Snowglobe.Services;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    public static event Action OnStart = delegate { };
    public static event Action OnLose = delegate { };
    public static event Action OnFinish = delegate { };

    public static event Action<bool> OnRestartLevel = delegate { }; // Event on restart the level
    public static event Action OnPauseGame = delegate { }; // Event on pause game;
    public static event Action<string, string> ShowMessage = delegate { }; // Event that show the two different messages on loose and finish.

    public Generator generator;
    public ObjectsPool objectsPool;
    public Image restartTimer;     //Эту картинку нужно еще создать
    public GameObject comboScore;
    public int comboCount = 0;
    
    private float timmer = 0;
    private float waitTillRestart = 10f;
    private bool switchState = true;
    private int recordScore = 0;
    private int currentLevel = 0;
   
    
    [HideInInspector] public bool resumeState = false;

    #region Saving variables
    [HideInInspector] public int levelNumber = 1;
    [HideInInspector] public int score = 0;
    [HideInInspector] public int bestScore = 0;
    #endregion

    #region Canvas variales
    public Slider levelProgress;
    public GameObject messageUI;

    [SerializeField] GameObject bottomButtons;
    [SerializeField] GameObject mainButtons;
    [SerializeField] GameObject settingsButtons;
    [SerializeField] GameObject movieButton;

    [SerializeField] Text menuMsgMainText;
    [SerializeField] Text menuMsgNewRecordText;
    [SerializeField] Text menuMsgButtText;
    [SerializeField] Text textCurrentScore;
    [SerializeField] Text textBestScore;
    [SerializeField] Text textCurrentLevel;
    [SerializeField] Text textNextLevel;
    [SerializeField] Text _textMovieButton;
    #endregion

    [Space]

    #region Material variables
    [SerializeField] Material safeZoneMaterial;         
    [SerializeField] Material cylinderMaterial;
    [SerializeField] Material ballMaterial;
    [SerializeField] Material trailMaterial;
    [SerializeField] Material fireStartMaterial;
    [SerializeField] Material splatterMaterial;
    #endregion

    // Drastic Fun Changes
    [SerializeField] private Game _game;
    private App _app;
    [SerializeField] private Ball _ball;
    public bool Revived = false;
    private bool _requestedRevive = false;
    
    public GameManager()
    {
        if (instance == null)
            instance = this;
    }

    private void OnEnable()
    {
        instance = this;
        _app = _game.App;
        generator.numberOfPlatforms = PlayerPrefs.GetInt("NumberOfPlatforms", generator.numberOfPlatforms);
        generator.platformDifficult = PlayerPrefs.GetInt("PlatformDifficult", generator.platformDifficult);
        bestScore = PlayerPrefs.GetInt("BestScore", bestScore);
        levelNumber = PlayerPrefs.GetInt("LevelNumber", levelNumber);
        //Ball.showMessage += Message;
        ShowMessage += OnPauseMessage;
        Ball.OnDie += GameOver;
        Ball.OnPlatformPassed += SpawnComboScore;
        Ball.OnPlatformPassed += ScoreCount;
        Ball.OnFinish += LevelCount;
        Ball.OnFinish += LevelPassed;
        //Ball.OnFinish += SetLevelColor;
    }
    
    private void OnDisable()
    {
        PlayerPrefs.SetInt("NumberOfPlatforms", generator.numberOfPlatforms);
        PlayerPrefs.SetInt("PlatformDifficult", generator.platformDifficult);
        PlayerPrefs.SetInt("BestScore", bestScore);
        PlayerPrefs.SetInt("LevelNumber", levelNumber);
        PlayerPrefs.Save();

        ShowMessage -= OnPauseMessage;
        Ball.OnDie -= GameOver;
        Ball.OnPlatformPassed -= SpawnComboScore;
        Ball.OnPlatformPassed -= ScoreCount;
        Ball.OnFinish -= LevelCount;
        Ball.OnFinish -= LevelPassed;
        //Ball.OnFinish -= SetLevelColor;
    }

    /// <summary>
    /// Saving Data method
    /// </summary>
    void SavingData()
    {
        PlayerPrefs.SetInt("NumberOfPlatforms", generator.numberOfPlatforms);
        PlayerPrefs.SetInt("PlatformDifficult", generator.platformDifficult);
        PlayerPrefs.SetInt("BestScore", bestScore);
        PlayerPrefs.SetInt("LevelNumber", levelNumber);
        PlayerPrefs.Save();
    }


    private void Start()
    {
        instance = this;
        LevelCount(false);
        ScoreCount(false);

        levelProgress.maxValue = generator.numberOfPlatforms - 1f;
        resumeState = false;

        SetPillowColor();
        SetBallColor();
        SetCylinderColor();
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if (Input.touchCount > 0)
        {
            if (bottomButtons.activeSelf == true && Mathf.Abs(Input.GetTouch(0).deltaPosition.x) > 4f)
            {
                bottomButtons.SetActive(false);
                OnStart(); // On Start event
            }
                
        }
        
    }

    void SpawnComboScore(bool state)
    {
        GameObject go = Instantiate(comboScore) as GameObject;
        go.transform.SetParent(textCurrentScore.transform);
        go.transform.position = new Vector3(textCurrentScore.transform.position.x, textCurrentScore.transform.position.y - 150, textCurrentScore.transform.position.z);
    }
    
    IEnumerator AutoRestart()
    {
        movieButton.SetActive(true); // Activation of Movie button
        restartTimer.fillAmount = 0;
        timmer = 0f;

        
        Act scaleUp = () => movieButton.transform.DOScale(1.1f, 0.25f);
        Act scaleDown = () => movieButton.transform.DOScale(0.95f, 0.25f);
        Act scaleNormal = () => movieButton.transform.DOScale(1.0f, 0.125f);
        
        _textMovieButton.text = "5";
        yield return new WaitForSeconds(1.0f);
        _textMovieButton.text = "4";
        scaleUp();
        yield return new WaitForSeconds(0.25f);
        scaleDown();
        yield return new WaitForSeconds(0.25f);
        scaleNormal();
        yield return new WaitForSeconds(0.5f);
        _textMovieButton.text = "3";
        scaleUp();
        yield return new WaitForSeconds(0.25f);
        scaleDown();
        yield return new WaitForSeconds(0.25f);
        scaleNormal();
        yield return new WaitForSeconds(0.5f);
        _textMovieButton.text = "2";
        scaleUp();
        yield return new WaitForSeconds(0.25f);
        scaleDown();
        yield return new WaitForSeconds(0.25f);
        scaleNormal();
        yield return new WaitForSeconds(0.5f);
        _textMovieButton.text = "1";
        scaleUp();
        yield return new WaitForSeconds(0.25f);
        scaleDown();
        yield return new WaitForSeconds(0.25f);
        scaleNormal();
        yield return new WaitForSeconds(0.5f);

        restartTimer.fillAmount = 0;
        timmer = 0f;
        movieButton.SetActive(false); // Deactivation of Movie button

        if(!_requestedRevive)
            RestartGame();
        yield return null;
    }

    /// <summary>
    /// Method for switch buttons on bottom screen
    /// </summary>
    public void SwitchButtons()
    {
        if (switchState)
        {
            mainButtons.SetActive(false);
            settingsButtons.SetActive(true);
            switchState = false;
        }
        else
        {
            mainButtons.SetActive(true);
            settingsButtons.SetActive(false);
            switchState = true;
        }
    }
    /// <summary>
    /// Counting and displaying score and best score
    /// </summary>
    /// <param name="state"></param>
    void ScoreCount(bool state)
    {
        if(state)
        {
            score += levelNumber;
            levelProgress.value += 1.0f;
            levelProgress.value = Mathf.Round(levelProgress.value);
        }
        
        
        if (bestScore < score)
        {
            bestScore = score;
        }

        textCurrentScore.text = score.ToString();
        textBestScore.text = "Best Score: " + bestScore.ToString();
    }

    /// <summary>
    /// Clear Score and Level progress bar data
    /// </summary>
    void ClearScore()
    {
        currentLevel = levelNumber;
        levelProgress.value = 0f;
        levelProgress.value = Mathf.Round(levelProgress.value);
        textCurrentScore.text = score.ToString();
    }

    /// <summary>
    /// Counting level number and displaying on screen
    /// </summary>
    /// <param name="state"></param>
    void LevelCount(bool state)
    {

        
        if (state)
        {
            levelNumber += 1;
        } else
        {
            currentLevel = levelNumber;
        }
        

        textCurrentLevel.text = levelNumber.ToString();
        textNextLevel.text = (levelNumber + 1).ToString();
    }

    /// <summary>
    /// Displaying Game Over text
    /// </summary>
    void GameOver()
    {
        _ball.Freeze();
        int percentage = Mathf.RoundToInt(levelProgress.value * 100f / levelProgress.maxValue);
        string gameOverText = percentage.ToString() + "% PASSED";
        
        if (bestScore == score)
        {
            _app.Services.Get<Leaderboards>().Report(bestScore);
            menuMsgNewRecordText.text = "NEW RECORD: " + bestScore.ToString();
        } else
        {
            menuMsgNewRecordText.text = "";
        }

        switchState = true;
        OnPauseMessage(gameOverText, "Tap to Try again");
        
        // DrasticFun Leaderboards Addition
//        int currentPoints = (int)_app.Stats.Get("points", 0f);
//        currentPoints += score;
//        _app.Services.Get<Leaderboards>().Report(currentPoints);
//        _app.Stats.Set("points", currentPoints);
        // --
        _requestedRevive = false;
    }
    void LevelPassed(bool state)
    {
        _ball.Freeze();
        movieButton.SetActive(false);
        
        string levelPassedText = levelNumber.ToString() + " Level passed";
        switchState = false;
        OnPauseMessage(levelPassedText, "Next level");
    }
    /// <summary>
    /// Method that triggered after player click on movie button
    /// </summary>
    public void ResumeGame()
    {
        _requestedRevive = false;
        _ball.UnFreeze();
    }

    public void RequestRevive()
    {   
        StopAllCoroutines();
        _requestedRevive = true;
    }

    public void ReviveGame()
    {
        _requestedRevive = false;
        Revived = true;
        _ball.Freeze();
        Vector3 newPos = _ball.transform.position;
        newPos.y += 2f;
        _ball.transform.position = newPos;
        Time.timeScale = 1f;
        CameraFocus.startFollow = true;
        levelProgress.maxValue = generator.numberOfPlatforms - 1f;
        messageUI.SetActive(false);
        StopAllCoroutines();
        switchState = false;
        resumeState = true;
        restartTimer.fillAmount = 0;
        movieButton.SetActive(false);
    }

    /// <summary>
    /// Restart game method
    /// </summary>
    public void RestartGame()
    {
        score = 0;
        instance = this;
        Revived = false;
        _ball.UnFreeze();
        Time.timeScale = 1f;
        CameraFocus.startFollow = true;
        levelProgress.maxValue = generator.numberOfPlatforms - 1f;
        mainButtons.SetActive(true);
        settingsButtons.SetActive(false);
        switchState = true;
        bottomButtons.SetActive(true);
        messageUI.SetActive(false);
        StopAllCoroutines();

        if (currentLevel < levelNumber)
            SetLevelColor();

        ClearScore();
        OnFinish(); // On Finish event
        OnPauseGame();
        OnRestartLevel(false);
    }

    /// <summary>
    /// Takes main message and button text to display on screen 
    /// </summary>
    /// <param name="mainMessage"></param>
    /// <param name="buttonMessage"></param>
    public void OnPauseMessage(string mainMessage, string buttonMessage)   
    {
        StopAllCoroutines();
        SavingData();
        OnLose(); // On Lose event

        if (switchState && !Revived)
            StartCoroutine(AutoRestart());
        else
        {
            StopAllCoroutines();
            movieButton.gameObject.SetActive(false);
        }

        
        messageUI.SetActive(true);
        menuMsgMainText.text = mainMessage;
        menuMsgButtText.text = buttonMessage;
    }

    #region Random Color generation

    Color pillowColor;
    Color cylinderColor;

    void SetLevelColor()
    {
        SetPillowColor();
        SetBallColor();
        SetCylinderColor();
    }
    /// <summary>
    /// Set Random Color for Platform
    /// </summary>
    public void SetPillowColor()
    {
        pillowColor = RandomColor();
        safeZoneMaterial.SetColor("_Color", pillowColor);
    }

    /// <summary>
    /// Set Random Color for Ball, trail and splatters
    /// </summary>
    public void SetBallColor()
    {
        Color ballColor = RandomColor();

        while(pillowColor == ballColor || cylinderColor == ballColor)
        ballColor = RandomColor();

        ballMaterial.SetColor("_Color", ballColor);
        trailMaterial.SetColor("_Color", ballColor);
        splatterMaterial.SetColor("_TintColor", ballColor);
        fireStartMaterial.SetColor("_TintColor", ballColor);
    }

    /// <summary>
    ///  Set Random Color for Pillar
    /// </summary>
    public void SetCylinderColor()
    {
        cylinderColor = RandomColor();

        while (pillowColor == cylinderColor)
            cylinderColor = RandomColor();

        cylinderMaterial.SetColor("_Color", cylinderColor);
    }

    Color RandomColor()
    {
        Color c = new Color();
        switch (UnityEngine.Random.Range(1, 7))
        {
            case 1: c = Color.black; break;
            case 2: c = Color.blue; break;
            case 3: c = Color.cyan; break;
            case 4: c = Color.yellow; break;
            case 5: c = Color.green; break;
            case 6: c = Color.white; break;
            case 7: c = Color.gray; break;
        }
        return c;
    }

    #endregion
}
