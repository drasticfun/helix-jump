﻿Shader "Unlit/BallDeformation"
{
	Properties
	{
        _Color ("Color", Color) = (0, 0, 0, 0)
        _Squishyness ("Squishness", float) = 0.25 
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"

			struct InputData
			{
				float4 vertex : POSITION;
                float4 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct OutputData
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

            float4 _Color;
            float _Squishyness;
			
			OutputData vert (InputData input)
			{
                OutputData output;

                float3 normal = mul(unity_ObjectToWorld, input.normal);
                normal.y = 0;

                /*if (!(normal.x == 0 && normal.z == 0)) {
                    normal = normalize(normal);
                }*/

                input.vertex.xyz += _Squishyness * normal * sin(_Time.w * 3);
                input.vertex.y -= _Squishyness * 0.5f * sin(_Time.w * 2);

                output.vertex = UnityObjectToClipPos(input.vertex);

				return output;
			}
			
			fixed4 frag (OutputData input) : Color
			{
				// sample the texture
				return _Color;
			}
			ENDCG
		}
	}
}
