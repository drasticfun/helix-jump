﻿using System;
using UnityEngine;

namespace Snowglobe.Utils
{
    public static class ResourceLoader
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string LoadText(string filePath)
        {
            // Load the text from resources
            TextAsset textAsset = Resources.Load(filePath) as TextAsset;
            
            // If the text not found, throw a ResourceNotFoundException
            if(!textAsset)
                throw new ResourceNotFoundException();
            
            // Return the found text
            return textAsset.text;
        }
    }

    public class ResourceNotFoundException : Exception
    {
        
    }
}