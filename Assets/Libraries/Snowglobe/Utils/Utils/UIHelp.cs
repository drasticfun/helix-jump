﻿using DG.Tweening;
using Snowglobe.Core;
using UnityEngine;

namespace Snowglobe.Utils
{
    public static class UIHelp
    {
        public enum OffscreenPositions
        {
            Top,
            Bottom,
            Left,
            Right
        }

        public static void AnimateToCenter(this RectTransform rect, OffscreenPositions pos, Act onComplete){
            AnimateToCenter(rect, pos, 0.5f, Ease.OutSine, onComplete);
        }

        public static void AnimateToCenter(this RectTransform rect, OffscreenPositions pos, float duration = 0.8f, Ease ease = Ease.OutSine, Act onComplete = null)
        {
            rect.DOKill();
            Vector2 center = Vector2.zero;
            rect.anchoredPosition = center;

            float rectMinX = 0;
            float rectMinY = 0;
            float rectMaxX = 0;
            float rectMaxY = 0;
            float rectMinTargetX = 0;
            float rectMinTargetY = 0;
            float rectMaxTargetX = 0;
            float rectMaxTargetY = 0;
            
            if (pos == OffscreenPositions.Top)
            {
                rectMinX = 0f;
                rectMaxX = 1f;
                rectMinTargetX = 0f;
                rectMaxTargetX = 1.0f;

                rectMinY = 1.2f;
                rectMaxY = 1.2f;
                rectMinTargetY = 0.5f;
                rectMaxTargetY = 0.5f;
            }
            else if (pos == OffscreenPositions.Bottom) {
                rectMinX = 1.0f;
                rectMaxX = 1.0f;

                rectMinY = 1.2f;
                rectMaxY = 1.2f;
                
                rectMinTargetX = 0f;
                rectMaxTargetX = 1.0f;
                
                rectMinTargetY = 0.5f;
                rectMaxTargetY = 0.5f;
            }
            else if (pos == OffscreenPositions.Left)
            {
                rectMinX = -1.2f;
                rectMaxX = -1.2f;
                rectMinTargetX = 0.5f;
                rectMaxTargetX = 0.5f;

                rectMinY = 0f;
                rectMaxY = 1f;
                rectMinTargetY = 0f;
                rectMaxTargetY = 1f;
            }
            else if (pos == OffscreenPositions.Right)
            {
                rectMinX = 1.0f;
                rectMaxX = 1.0f;

                rectMinY = 1.2f;
                rectMaxY = 1.2f;
                
                rectMinTargetX = 0f;
                rectMaxTargetX = 1.0f;
                
                rectMinTargetY = 0.5f;
                rectMaxTargetY = 0.5f;             
            }

            rect.anchorMin = new Vector2(rectMinX, rectMinY);
            rect.anchorMax = new Vector2(rectMaxX, rectMaxY);
            rect.DOAnchorMin(new Vector2(rectMinTargetX, rectMinTargetY), duration).SetEase(ease).onComplete += () =>
            {
                if (onComplete != null)
                    onComplete();
            };
            rect.DOAnchorMax(new Vector2(rectMaxTargetX, rectMaxTargetY), duration).SetEase(ease);

        }

        public static void AnimateToOffscreen(this RectTransform rect, OffscreenPositions pos, float delay = 2.0f,float duration = 0.8f, Ease ease = Ease.OutSine, Act onComplete = null)
        {
            rect.DOKill();
            
            float targetMinX = 0f;
            float targetMinY = 1.2f;
            
            float targetMaxX = 1.0f;
            float targetMaxY = 1.2f;
            
            if (pos == OffscreenPositions.Top)
            {
                targetMinX = 0f;
                targetMaxX = 1.0f;
                
                targetMinY = 1.2f;
                targetMaxY = 1.2f;
            }
            else if (pos == OffscreenPositions.Bottom) {
                targetMinX = 0f;
                targetMaxX = 1.0f;
                
                targetMinY = 1.2f;
                targetMaxY = 1.2f;
            }
            else if (pos == OffscreenPositions.Left)
            {
                targetMinX = 0f;
                targetMaxX = 1.0f;
                
                targetMinY = 1.2f;
                targetMaxY = 1.2f;
            }
            else if (pos == OffscreenPositions.Right)
            {
                targetMinX = 1.5f;
                targetMaxX = 1.5f;
                
                targetMinY = 0f;
                targetMaxY = 1f;          
            }
            
            rect.DOAnchorMin(new Vector2(targetMinX, targetMinY), duration).SetEase(ease).SetDelay(delay);
            rect.DOAnchorMax(new Vector2(targetMaxX, targetMaxY), duration).SetEase(ease).SetDelay(delay).onComplete += () =>
            {
                if (onComplete != null)
                    onComplete();
            };
        }
    }
}