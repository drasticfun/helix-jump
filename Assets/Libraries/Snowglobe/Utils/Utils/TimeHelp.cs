﻿using System;

namespace Snowglobe.Utils
{
    public static class TimeHelp
    {
        public static DateTime Current
        {
            get
            {
                return DateTime.UtcNow;
            }            
        }

        public static DateTime StartOfDay
        {
            get
            {
                DateTime currentTime = Current;
                DateTime startOfDay = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 0, 0, 0);
                return startOfDay;
            }
        }

        public static TimeSpan GetTimeSince(DateTime time)
        {
            TimeSpan since = TimeSpan.FromTicks(Current.Ticks - time.Ticks);
            return since;
        }
        
        public static TimeSpan GetTimeSince(long ticks)
        {
            return GetTimeSince(new DateTime(ticks));
        }
    }
}