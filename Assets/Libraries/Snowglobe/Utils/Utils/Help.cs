﻿using System;
using System.Collections.Generic;
using Foot;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

namespace Snowglobe.Utils
{
    public static class Help
    {
        public static Bounds getBounds(this Transform obj, bool recursive = true)
        {
            if (recursive)
            {
                Bounds bounds;
                Renderer childRender;
                bounds = getBounds(obj, false);
                if (bounds.extents.x == 0)
                {
                    bounds = new Bounds(obj.transform.position, Vector3.zero);
                    foreach (Transform child in obj.transform)
                    {
                        childRender = child.GetComponent<Renderer>();
                        if (childRender)
                        {
                            bounds.Encapsulate(childRender.bounds);
                        }
                        else
                        {
                            bounds.Encapsulate(getBounds(child));
                        }
                    }
                }

                return bounds;
            }

            else
            {
                Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
                Renderer render = obj.GetComponent<Renderer>();
                if (render != null)
                {
                    return render.bounds;
                }

                return bounds;
            }
        }

        /// <summary>
        /// Our default static random
        /// </summary>
        private static Random _random = new Random();

        public static float rand(float from, float to)
        {
            return UnityEngine.Random.Range(from, to);
        }

        public static int rand(int from, int to)
        {
            return UnityEngine.Random.Range(from, to);
        }

        public static bool rand(float truePrecent)
        {
            return UnityEngine.Random.Range(1, 101) <= truePrecent;
        }

        public static T rand<T>(T[] arr)
        {
            return arr[(int) rand(0f, arr.Length)];
        }

        public static Vector3 rand(Vector3 from, Vector3 to)
        {
            float randX = rand(from.x, to.x);
            float randY = rand(from.y, to.y);
            float randZ = rand(from.z, to.z);

            return new Vector3(randX, randY, randZ);
        }

        /// <summary>
        /// Gets a random number between
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="probabilityPower"></param>
        /// <returns></returns>
        public static int getRandomNumber(int max, int min, double probabilityPower = 2)
        {
            var randomDouble = _random.NextDouble();
            var result = Math.Floor(min + (max + 1 - min) * (Math.Pow(randomDouble, probabilityPower)));
            return (int) result;
        }

        /// <summary>
        /// Performs an action on each child of obj
        /// </summary>
        public static void recursiveAction(GameObject obj, ChildAction action)
        {
            // Iterate childs recursively
            for (int i = 0; i < obj.transform.childCount; i++)
            {
                GameObject child = obj.transform.GetChild(i).gameObject;
                recursiveAction(obj, action);
            }

            // Perform the action
            action(obj);
        }

        // Delegate of action on childs
        public delegate void ChildAction(GameObject obj);

        /// <summary>
        /// Instantiates prefab under parent
        /// </summary>
        /// <param name="prefab">
        /// Prefab to spawn
        /// </param>
        /// <param name="parent">
        /// New parent of spawned prefab
        /// </param>
        /// <param name="activate">
        /// Should the object be activated?
        /// </param>
        /// <param name="keepPos">
        /// Prevents problems with ui layouts 
        /// </param>
        /// <returns></returns>
        public static GameObject instantiate(GameObject prefab, Transform parent, Boolean activate = true,
            Boolean keepPos = true)
        {
            // Instantiate our gameobject
            GameObject obj = Object.Instantiate(prefab);
            // Set its parent
            obj.transform.SetParent(parent, keepPos);

            // Set obj activated
            obj.SetActive(activate);

            // return our newly spawned obj
            return obj;
        }

        /// <summary>
        /// Casts an object-object dictionary to TKey,TVal
        /// </summary>
        public static IDictionary<TKey, TVal> castDictionary<TKey, TVal>(IDictionary<object, object> dict)
        {
            IDictionary<TKey, TVal> convertedDictionary = new Dictionary<TKey, TVal>();
            foreach (KeyValuePair<object, object> keyValuePair in dict)
            {
                object key = keyValuePair.Key;
                object val = keyValuePair.Value;
                convertedDictionary.Add((TKey) key, (TVal) val);
            }

            return convertedDictionary;
        }

        /// <summary>
        /// Extract random character from text
        /// </summary>
        /// <param name="text"></param>
        public static char getRandomCharacter(string text)
        {
            int index = _random.Next(text.Length);
            return text[index];
        }

        public static T Spawn<T>(T prefab, Transform parent, bool keepPos = true) where T:MonoBehaviour
        {
            return instantiate(prefab.gameObject, parent, true, keepPos).GetComponent<T>();
        }

        
        public static GameObject Spawn(GameObject prefab, Transform parent, bool keepPos = true)
        {
            return instantiate(prefab, parent, true, keepPos);
        }

        
        public static void RemoveChildOfType<T>(Transform transform) where T : MonoBehaviour
        {
            Transform targetChild = null;

            for (int i = 0; i < transform.childCount; i++)
            {
                T comp = transform.GetChild(i).GetComponent<T>();
                if (comp != null)
                    targetChild = comp.transform;
            }

            if (targetChild != null)
            {        
                GameObject.Destroy(targetChild.gameObject);
            }
        }

        public static void RemoveAll(Transform transform)
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    static class RandomExtensions
    {
        public static void Shuffle<T>(this Random rng, T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
    
    static class TransformExtensions
    {
        public static T Add<T>(this Transform transform, T prefab, bool keepPos = true) where T:MonoBehaviour
        {
            return Help.Spawn(prefab, transform, keepPos);
        }
        
        public static GameObject Add(this Transform transform, GameObject prefab, bool keepPos = true)
        {
            return Help.Spawn(prefab, transform, keepPos);
        }
        
        public static T AddLocal<T>(this Transform transform, T prefab) where T:MonoBehaviour
        {
            T obj = Help.Spawn(prefab, transform);
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localEulerAngles = Vector3.zero;
            obj.transform.localScale = Vector3.one;
            return obj;
        }
        
        public static void Remove<T>(this Transform transform) where T:MonoBehaviour
        {
            Help.RemoveChildOfType<T>(transform);
        }

        public static void Clear(this Transform transform)
        {
            Help.RemoveAll(transform);
        }
    }
}