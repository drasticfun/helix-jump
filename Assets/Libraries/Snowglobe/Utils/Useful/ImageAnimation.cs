﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Snowglobe.Extra
{
    public class ImageAnimation : MonoBehaviour
    {
        [SerializeField] private Sprite[] _sprites;
        [SerializeField] private float _interval;

        private Image _image;
        private int _currIndex = 0;


        private void Awake(){
            _image = GetComponent<Image>();
        }

        private void OnEnable(){
            _currIndex = -1;
            StartCoroutine(animateImage());
        }

        private void OnDisable(){
            StopCoroutine(animateImage());
        }

        public IEnumerator animateImage(){
            while (true){
                nextImage();
                yield return new WaitForSeconds(_interval);
            }
        }

        private void nextImage(){
            _currIndex++;
            if (_currIndex >= _sprites.Length)
                _currIndex = 0;
            _image.sprite = _sprites[_currIndex];
        }
    }
}