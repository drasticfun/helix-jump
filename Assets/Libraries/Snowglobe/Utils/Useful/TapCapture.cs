﻿using Snowglobe.Core;
using Snowglobe.Game;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Snowglobe.Extra
{   
    public class TapCapture : Object<Session>
    {
        public event Act OnTap = delegate {  };
        
        Button _capture; 
        
        protected override void preInitialize()
        {
            base.preInitialize();

            _capture = gameObject.AddComponent<Button>();
            _capture.transition = Selectable.Transition.None;
            //_capture.onClick.AddListener(invokeEvent);
            
            EventTrigger trigger = _capture.gameObject.AddComponent<EventTrigger>();
            
            var pointerDown = new EventTrigger.Entry();
            pointerDown.eventID = EventTriggerType.PointerDown;
            pointerDown.callback.AddListener(invokeEvent);
            trigger.triggers.Add(pointerDown);
        }

        private void invokeEvent(BaseEventData arg0)
        {
            OnTap();
        }
    }
}