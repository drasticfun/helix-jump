﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace Snowglobe.Generators
{
    [CreateAssetMenu]
    public class GradientMaker : ScriptableObject
    {
        [SerializeField] private UnityEngine.Gradient _gradient = new UnityEngine.Gradient();
        [SerializeField] private int _width = 256;
        [SerializeField] private int _height = 2;
        [SerializeField] Texture2D _targetTexture;

        /// <summary>
        /// Creates the texture
        /// </summary>
        /// <returns></returns>
        public Texture2D make(){
            // The Texture were creating
            Texture2D texture = new Texture2D(_width, _height);

            for (int i = 0; i < _width; i++){
                var c = _gradient.Evaluate(((float) i) / _width);
                for (int j = 0; j < _height; j++){
                    texture.SetPixel(i, j, c);
                }
            }

            texture.Apply();
            return texture;
        }

        [ContextMenu("Apply")]
        public void apply(){
            string path;
            if (_targetTexture == null){
                var assetPath = AssetDatabase.GetAssetPath(this);
                path = assetPath.Substring(0, assetPath.Length - 5) + "png";
            }
            else{
                path = AssetDatabase.GetAssetPath(_targetTexture);
            }

            var tex = make();
            var data = tex.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, data);
            AssetDatabase.ImportAsset(path);
            DestroyImmediate(tex);
            _targetTexture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
        }
    }

    [CustomEditor(typeof(GradientMaker))]
    public class GradientEditor : Editor
    {
        public override void OnInspectorGUI(){
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("gradient"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("width"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("height"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("targetTexture"));
            if (GUILayout.Button("Apply")){
                foreach (var item in targets){
                    (item as GradientMaker).apply();
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif