﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FadeDestroy : MonoBehaviour
{
	[SerializeField]
	private float _duration = 2f;
	[SerializeField]
	private Image _image;
	
	// Use this for initialization
	void Start (){
		_image.DOFade(0f, _duration).onComplete += Destroy;
	}

	private void Destroy(){
		Destroy(gameObject);
	}
}
