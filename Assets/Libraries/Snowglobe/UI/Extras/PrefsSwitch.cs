﻿using Snowglobe.Core;
using UnityEngine;
using UnityEngine.UI;

public class PrefsSwitch : Object<App>
{
    // Config //
    // The settings to toggle
    [SerializeField] private string _settingsKey;

    // Image - Button will be added to this image
    [SerializeField] private Image _image;

    // Sprite when enabled
    [SerializeField] private Sprite _enabledSprite;

    // Sprite when disabled
    [SerializeField] private Sprite _disabledSprite;

    // Instance //    
    // Is the settins enabled?
    private bool _enabled = false;

    protected override void initialize()
    {
        GetComponent<Button>().onClick.AddListener(Toggle);
        Refresh();
    }

    protected override void onActive()
    {
        App.Prefs.OnUpdate += Refresh;
    }
    
    protected override void onDeactive()
    {
        App.Prefs.OnUpdate -= Refresh;
    }
   
    private void Refresh()
    {
        _enabled = App.Prefs.Get(_settingsKey);
        _image.sprite = _enabled ? _enabledSprite : _disabledSprite;
    }

    private void Toggle()
    {
        App.Prefs.Toggle(_settingsKey);
    }
}