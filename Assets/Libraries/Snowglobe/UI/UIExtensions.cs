﻿using UnityEngine;
using UnityEngine.UI;

namespace Snowglobe.UI
{
    public static class ImageExtensions
    {
        public static void SetAlpha(this Image image, float a)
        {
            Color color = image.color;
            color.a = a;
            image.color = color;
        }
    }
}