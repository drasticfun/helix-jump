﻿using DG.Tweening;
using Snowglobe.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Foot.Game
{
    // ReSharper disable once InconsistentNaming
    public class UITransition : Object<App>
    {
        #region Visual Config

        [Header("Transition Config")] [SerializeField]
        private float _backgroundFade = 0.5f;

        [SerializeField] private float _animationDuration = 0.4f;

        #endregion

        #region Components

        private RectTransform _rect;
        private Image _image;

        #endregion

        protected override void onActive()
        {
            Enter();
        }

        public void Enter()
        {
            Vector2 pos = Vector2.zero;
            pos.y = 0;
            _rect.anchoredPosition = pos;
            _rect.anchorMin = new Vector2(0f, 1.2f);
            _rect.anchorMax = new Vector2(1f, 1.2f);

            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0);
            _image.DOFade(255f * _backgroundFade, _animationDuration);
            _rect.DOAnchorMin(new Vector2(0f, 0.5f), _animationDuration);
            _rect.DOAnchorMax(new Vector2(1f, 0.5f), _animationDuration);
        }

        public void Exit(bool deactivateOnFinish = true)
        {
            _image.DOFade(0f, _animationDuration);
            _rect.DOAnchorMin(new Vector2(0f, 1.2f), _animationDuration);
            _rect.DOAnchorMax(new Vector2(1f, 1.2f), _animationDuration).onComplete += () =>
            {
                if (deactivateOnFinish) Deactivate();
            };
        }
    }
}