﻿using Coffee.UIExtensions;
using DG.Tweening;
using Snowglobe.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Session
{
    public class ScreenEffects : Service
    {
        [SerializeField] private Image _fader;

        [SerializeField] private Image _flash;
        [SerializeField] private UIEffect _flashEffects;
        [SerializeField] private float _flashStartAlpha = 0;
        [SerializeField] private Color _flashColor;
        [SerializeField] private Ease _flashInEase;
        [SerializeField] private Ease _flashOutEase;
        [SerializeField] private float _flashInDuration;
        [SerializeField] private float _flashOutDuration;

        public void flash(){
            flash(_flashColor, _flashInDuration, _flashOutDuration);
        }

        public void flash(Color color){
            flash(color, _flashInDuration,
                _flashOutDuration, _flashInEase, _flashOutEase);
        }

        public void flash(Color color, float inDuration, float outDuration){
            flash(color, inDuration,
                outDuration, _flashInEase, _flashOutEase);
        }

        public void flash(Color color, float inDuration, float outDuration, Ease inEase, Ease outEase){
            _flash.DOKill();
            _flash.color = new Color(_flash.color.r, _flash.color.g, _flash.color.b, _flashStartAlpha);
            Color c = new Color(_flash.color.r, _flash.color.g, _flash.color.b, 87.0f/255.0f);
            
            _flashEffects.effectColor = color;
            _flash.DOColor(c, inDuration).SetEase(inEase).onComplete += () =>
            {
                _flash.DOFade(0, outDuration).SetEase(outEase);
            };
        }

        public void shake(float duration = 0.5f, float strength = 0.7f){
            Camera.main.DOShakePosition(duration, strength);
        }

        public void fade(float duration, Act onFaded){
            _fader.DOFade(1, duration).onComplete += () => {
                onFaded();
                _fader.DOFade(0, duration).SetDelay(1.0f);
            };
        }
    }
}