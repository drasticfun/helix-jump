﻿//using System.Collections.Generic;
//using Code.Constants;
//using DG.Tweening;
//using Snowglobe.Core;
//using Snowglobe.Services;
//using Snowglobe.Utils;
//using TapticPlugin;
//using UnityEngine;
//using UnityEngine.UI;
//
//public class StarsRewarder : Object<App>
//{
//    #region Services
//
//    private Ads _ads;
//    private Sound _sound;
//
//    #endregion
//
//    #region StarsConfig
//
//    [Header("Stars Configuration")] [SerializeField]
//    private AudioClip _rewardStartSound;
//
//    [SerializeField] private AudioClip _starHitSound;
//    [SerializeField] private GameObject _starPrefab;
//    [SerializeField] private Text _earnedText;
//    [SerializeField] private Text _starsText;
//
//    #endregion
//
//    #region Components
//
//    private Image _image;
//
//    #endregion
//
//    #region Instance Variables
//
//    // will abort all checkStars calls
//    private bool _pauseChecking = false;
//
//    // last recorded stars count
//    private int _lastStarsCount = 0;
//
//    // spawned stars
//    private IList<GameObject> _spawnedStars;
//
//    #endregion
//
//    #region Lifecycle
//
//    protected override void preInitialize()
//    {
//        _image = GetComponent<Image>();
//
//        _sound = App.Services.Get<Sound>();
//        _ads = App.Services.Get<Ads>();
//
//        InitStars();
//    }
//
//    protected override void onActive()
//    {
//        _pauseChecking = false;
//        _image.raycastTarget = true;
//        Invoke("CheckStars", 0.7f);
//
//        App.Currencies.onUpdate += UpdateStars;
//        _ads.Video.onOpen += OnAdOpen;
//        _ads.Interstitial.onOpen += OnAdOpen;
//        _ads.Video.onClose += OnAdClose;
//        _ads.Interstitial.onClose += OnAdClose;
//    }
//
//    protected override void onDeactive()
//    {
//        App.Currencies.onUpdate -= UpdateStars;
//        _ads.Video.onOpen -= OnAdOpen;
//        _ads.Interstitial.onOpen -= OnAdOpen;
//        _ads.Video.onClose -= OnAdClose;
//        _ads.Interstitial.onClose -= OnAdClose;
//    }
//
//    #endregion
//
//    #region Stars
//
//    private void InitStars()
//    {
//        _lastStarsCount = (int) App.Currencies.Get(Currencies.Stars, 0);
//        _starsText.text = string.Format("{0}", _lastStarsCount);
//    }
//
//    // destroy all spawned stars
//    private void ClearStars()
//    {
//        if (_spawnedStars != null && _spawnedStars.Count > 0)
//        {
//            foreach (GameObject star in _spawnedStars)
//            {
//                if (star)
//                    Destroy(star.gameObject);
//            }
//        }
//    }
//
//    private void UpdateStars()
//    {
//        int stars = (int) App.Currencies.Get(Currencies.Stars, 0);
//        int lastCount = _lastStarsCount;
//
//        if (stars < lastCount)
//            _starsText.text = string.Format("{0}", stars);
//    }
//
//    private void CheckStars()
//    {
//        _image.raycastTarget = false;
//        if (_pauseChecking)
//            return;
//
//        int stars = (int) App.Currencies.Get(Currencies.Stars, 0);
//        int lastCount = _lastStarsCount;
//        _lastStarsCount = stars;
//
//        if (stars == lastCount)
//            return;
//
//        ClearStars();
//
////        log(string.Format("checkStars :: stars: {0}", stars));
////        log(string.Format("checkStars :: lastCount: {0}", lastCount));
//
//        // The user has earned some stars
//        if (stars > lastCount)
//        {
//            _image.raycastTarget = true;
//            Invoke("DisableRaycast", 1.5f);
//
//            _image.DOFade(0.5f, 0.7f).SetDelay(0f);
//            _image.DOFade(0f, 0.5f).SetDelay(2.0f);
//
//            int starsEarned = stars - lastCount;
//
//            _sound.Play(_rewardStartSound, 0.5f);
//
//            float delay = 0.8f;
//            int extras = 1;
//            int maxExtras = 50;
//            int currentStars = lastCount;
//
//            _spawnedStars = new List<GameObject>();
//            for (int i = 1; i <= Mathf.Clamp(starsEarned, 1, maxExtras); i++)
//            {
//                _spawnedStars.Add(Help.instantiate(_starPrefab, transform, true, false));
//            }
//
//            _earnedText.gameObject.SetActive(true);
//            _earnedText.text = string.Format("+{0}", starsEarned);
//            Color c = Color.white;
//            c.a = 0;
//            _earnedText.color = c;
//            _earnedText.transform.localScale = Vector3.one;
//            _earnedText.DOFade(1.0f, 0.3f);
//            _earnedText.transform.DOScale(1.5f, 0.3f).onComplete += () =>
//            {
//                _earnedText.transform.DOScale(1.3f, 0.15f);
//            };
//            int currentPoolStar = 0;
//            for (int i = 1; i <= starsEarned; i++)
//            {
//                currentStars++;
//                int closureStars = currentStars;
//
//                // Get Star from Pool
//                GameObject star = _spawnedStars[currentPoolStar];
//                currentPoolStar++;
//                if (currentPoolStar >= _spawnedStars.Count)
//                    currentPoolStar = 0;
//
//                var i1 = i;
//                star.transform.DOMove(_starsText.transform.position, 0.4f).SetDelay(delay).onComplete += () =>
//                {
//                    if (extras <= maxExtras)
//                    {
//                        extras++;
//                        TapticManager.Impact(ImpactFeedback.Light);
//                        _sound.Play(_starHitSound, 1.0f);
//
//                        _starsText.transform.DOKill();
//                        _starsText.transform.DOScale(1.2f, 0.2f).onComplete += () =>
//                        {
//                            _starsText.transform.DOScale(1.0f, 0.1f);
//                        };
//                        _starsText.text = string.Format("{0}", closureStars);
//                    }
//
//                    if (extras == maxExtras || extras == i1 + 1)
//                    {
//                        _earnedText.DOFade(0f, 0.3f).onComplete += () => { _earnedText.gameObject.SetActive(false); };
//
//                        star.transform.DOScale(1.0f, 0.2f).SetDelay(0.2f).onComplete += () =>
//                        {
//                            ClearStars();
//                            _starsText.text = string.Format("{0}", stars);
//                        };
//                    }
//                };
//
//                _spawnedStars.Add(star);
//
//                delay = Mathf.Clamp(delay + 0.05f, 0, 7f);
//            }
//        }
//        else
//            _image.raycastTarget = false;
//    }
//
//    #endregion
//
//    #region Ad Events
//
//    private void OnAdOpen()
//    {
//        _pauseChecking = true;
//    }
//
//    private void OnAdClose()
//    {
//        _pauseChecking = false;
//        Invoke("CheckStars", 0.5f);
//    }
//
//    #endregion
//
//    private void DisableRaycast()
//    {
//        _image.raycastTarget = false;
//    }
//}