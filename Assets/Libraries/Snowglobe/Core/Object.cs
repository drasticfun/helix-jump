﻿using System;
using Snowglobe.Core.Storage;
using UnityEngine;
using Snowglobe.Utils;

namespace Snowglobe.Core
{
    /// <summary>
    /// CREATE VECTOR EXTENSIONS!!!
    /// </summary>
    public static class VectorExtension
    {
        public static void upX(this Vector3 vec, int x){
            
        }
    }

    public abstract class Object : MonoBehaviour
    {
        private bool _initialized;

        public bool isInitialized
        {
            get { return _initialized; }
        }

        // Traverse parent until App is found
        public App App
        {
            get { return getApp(); }
        }

        /// <summary>
        /// Unity's Awake
        /// </summary>
        protected virtual void Awake()
        {
            preInitialize();
            _initialized = true;
        }

        /// <summary>
        /// Unity's Start
        /// </summary>
        protected virtual void Start()
        {
            UnityStart();
        }
        
        /// <summary>
        /// Invoked by Awake to notify
        /// that the object should initialize
        /// </summary>
        protected virtual void preInitialize()
        {
        }

        protected abstract void UnityStart();

        /// <summary>
        /// Activates gameObject
        /// </summary>
        public void Activate()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Deactivates gameObject
        /// </summary>
        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Useful for Object Pooling
        /// 
        /// TODO: Think about implementation details
        /// </summary>
        public void Reactivate()
        {
            onReactivate();
        }

        protected virtual void onReactivate()
        {
        }
        
        /// <summary>
        /// Traverse transform.parent until
        /// object of type App is found
        /// </summary>
        /// <returns> App or null </returns>
        public App getApp()
        {
            // Traverse up until we find
            // an object with a GameApp component
            Transform parent = transform.parent;
            while (parent != null)
            {
                App obj = parent.GetComponent<App>();
                if (obj != null)
                    return obj;

                // Iterate next parent
                parent = parent.parent;
            }

            return null;
        }

        /// <summary>
        /// Traverse transform.parent until
        /// object of type T is found
        /// </summary>
        /// <returns> T or null </returns>
        protected T getParent<T>() where T:Object
        {
            // Traverse up until we find
            // an object with a GameApp component
            Transform parent = transform.parent;
            while (parent != null)
            {
                T obj = parent.GetComponent<T>();
                if (obj != null)
                    return obj;

                // Iterate next parent
                parent = parent.parent;
            }

            return null;
        }

        public T getService<T>() where T : Service
        {
            return App.Services.Get<T>();
        }

        /// <summary>
        /// Adds a child GameObject
        /// and sets its parent to this.transform
        /// </summary>
        protected GameObject addChild(GameObject childObject)
        {
            GameObject spawnedChild = Help.instantiate(childObject, transform, true, false);
            return spawnedChild;
        }

        /// <summary>
        /// Uses addChild with supplied <T> 
        /// </summary>
        /// <returns></returns>
        protected T addChild<T>() where T : Component
        {
            Type compType = typeof(T);
            return addChild(compType) as T;
        }

        /// <summary>
        /// Creates a new GameObject
        /// with zero transform and
        /// attached component of type compType.
        /// </summary>
        /// <param name="compType">
        /// The type of the component to add
        /// </param>
        /// <returns>
        /// The added component
        /// </returns>
        private Component addChild(Type compType)
        {
            // Instantiate a blank gameObject
            GameObject child = new GameObject(compType.Name);

            // Set the child's parent
            child.transform.parent = transform;

            // Normalize Transform
            child.transform.localPosition = Vector3.zero;
            child.transform.localEulerAngles = Vector3.zero;
            child.transform.localScale = Vector3.one;

            // Add the component to the instantiated child
            return child.AddComponent(compType);
        }

        /// <summary>
        /// Adds a child prefab and returns
        /// its component of type T
        /// </summary>
        protected T addChild<T>(T prefab) where T : Component
        {
            return addChild(prefab.gameObject).GetComponent<T>();
        }

        /// <summary>
        /// Find child of type T
        /// </summary>
        /// <returns>
        /// The found child
        /// </returns>
        public T getChild<T>() where T : Component
        {
            return (T) getChild(typeof(T));
        }

        /// <summary>
        /// Finds a child with attached compType
        /// </summary>
        /// <returns>
        /// The found component
        /// </returns>
        private Component getChild(Type compType)
        {
            // Iterate child to find child with compType
            for (int i = 0; i < transform.childCount; i++)
            {
                Component comp = transform.GetChild(i).GetComponent(compType);

                // Found component
                if (comp != null)
                    return comp;
            }

            // Child with compType not found
            return null;
        }

        /// <summary>
        /// Destroy all children
        /// </summary>
        /// <param name="immediate">
        /// Should use DestroyImmediate
        /// </param>
        protected void clearChilds(bool immediate = false)
        {
            for (int childIndex = 0; childIndex < gameObject.transform.childCount; childIndex++)
            {
                GameObject child = gameObject.transform.GetChild(childIndex).gameObject;
                if (immediate)
                    DestroyImmediate(child);
                else
                    Destroy(child);
            }
        }

        protected void log(String message)
        {
            Debug.Log(getLogTag() + ": " + message);
        }

        protected void log(float message)
        {
            Debug.Log(getLogTag() + ": " + message);
        }

        protected void log(string message, params object[] args)
        {
            Debug.Log(getLogTag() + ": " + string.Format(message, args));
        }
        
        
        protected virtual String getLogTag()
        {
            return GetType().Name;
        }
        
        public static void Remove(GameObject target)
        {
            if(target != null)
                GameObject.Destroy(target.gameObject);
        }
        public static void Remove(MonoBehaviour target)
        {
            Remove(target.gameObject);
        }
    }

    public abstract class Object<T> : Object where T : Object
    {
        private bool _postInitialized;
        private T _lastParent;

        protected sealed override void Awake()
        {
            base.Awake();
        }

        protected virtual void initialize()
        {
        }
        
        protected override void UnityStart()
        {
            if (!_postInitialized)
            {
                initialize();
                onActive();
                _postInitialized = true;
            }
            
//            Enter();
        }

        private void OnEnable()
        {
            if(_postInitialized)
                onActive();
        } 

        private void OnDisable()
        {
            if(_postInitialized)
                onDeactive();
        }

        protected virtual void onActive()
        {
        }
        
        protected virtual void onDeactive()
        {
        }
        
//        protected virtual void Enter()
//        {
//        }

        private void invokeStart()
        {
//            Enter();
        }

        // Gets the closest parent of type T
        public T getParent()
        {
            if (!hasDynamicParent() && _lastParent != null)
                return _lastParent;


            getParent<T>();
            Transform parent = transform.parent;

            while (parent != null)
            {
                T tempObj = parent.GetComponent<T>();
                if (tempObj != null)
                {
                    _lastParent = tempObj;
                    return tempObj;
                }

                // Iterate next parent
                parent = parent.parent;
            }

            return default(T);
        }

        protected virtual bool hasDynamicParent()
        {
            return false;
        }
    }

    public class GenericObject: Object<App>
    {
//        protected Storage<float> Stats
//        {
//            get { return App.Stats; }
//        }
    }
}