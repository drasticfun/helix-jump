﻿using System;
using UnityEngine;

namespace Snowglobe.Core.Storage
{
    public class Preferences
    {
        private string _prefix;
        
        public event Act OnUpdate = delegate { };
        
        public Preferences(string prefix = "preferences")
        {
            _prefix = prefix + "_";
        }

        public void Set(String settingId, bool enable)
        {
            if (enable)
                PlayerPrefs.SetInt(_prefix + settingId, 1);
            else
                PlayerPrefs.SetInt(_prefix + settingId, 0);
            
            OnUpdate();
        }

        public bool Get(string settingId, bool def = false)
        {
            int enabled = PlayerPrefs.GetInt(_prefix + settingId, def ? 1 : 0);
            return enabled >= 1;
        }

        public void Enable(string key)
        {
            Set(key, true);
        }

        public void Disable(string key)
        {
            Set(key, false);
        }

        public void Toggle(string settingId)
        {   
            Set(settingId, !Get(settingId));
        }
    }
}