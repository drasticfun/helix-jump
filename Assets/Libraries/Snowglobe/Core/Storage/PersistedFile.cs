﻿using UnityEngine;

public class PersistedFile
{
    private string _fileName;
    private ES3File _file;
        
    public PersistedFile(string fileName)
    {
        _fileName = fileName;
        _file = new ES3File(_fileName);
    }

    public string getValue(string key, string defaultValue)
    {
        string value = _file.Load<string>(key, defaultValue);
        return value;
    }

    public T getValue<T>(string key, T defaultValue)
    {
        T value = _file.Load<T>(key, defaultValue);
        return value;
    }

    public void Sync()
    {
        _file.Sync();
    }

    public void SetValue<T>(string key, T value)
    {
        _file.Save<T>(key, value);
    }

    public bool HasKey(string key)
    {
        return _file.KeyExists(key);
    }
}