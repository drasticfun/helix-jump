﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Snowglobe.Core.Storage
{
    /// <summary>
    /// The Persistor is hard-linked to a single IPersistable
    /// At the initialization, the Persistor will load
    /// into the Dictionary using QuickSave's Library
    /// 
    /// Persistor will persist the IPersistable when it updates.
    /// </summary>
    public class Persistor
    {
        /// <summary>
        /// The name of the storage
        /// inside QuickSave's Root
        /// </summary>
        private string _storeName;

        /// <summary>
        /// The storage to persist
        /// </summary>
        private IPersistable _persistable;

        /// <summary>
        /// Save settings for QuickSave
        /// </summary>
//        private QuickSaveSettings _saveSettings;
        public Persistor(string storeName, IPersistable persistable){
            _storeName = storeName;
            _persistable = persistable;
//            _saveSettings = saveSettings;

            // Load storage
            load();

            // Persist on storage updates
            persistable.onUpdate += persist;
        }

        /// <summary>
        /// Loads the store into attached storage
        /// </summary>
        private void load(){
            IDictionary<string, string> loadedDictionary =
                ES3.Load(_storeName, new Dictionary<string, string>());

            _persistable.load(loadedDictionary);
        }

        /// <summary>
        /// Persist the storage by saving it
        /// </summary>
        private void persist(){
            ES3.Save<Dictionary<string, string>>(_storeName, _persistable.getDictionary());
        }
    }
}