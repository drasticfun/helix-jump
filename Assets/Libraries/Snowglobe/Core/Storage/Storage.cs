﻿//using System;
//using System.Collections.Generic;
//using Snowglobe.Core.Storage;
//
//namespace Snowglobe.Core
//{
//}
//namespace Snowglobe.Core.Storage
//{
//    /// <summary>
//    /// Store provides an interface for creating a Storage of sub-storages
//    /// 
//    /// Store is a shorthand for a Storage<object>
//    /// and introduce an extra get method
//    /// </summary>
////    public class Store : Storage<Storage<object>>, IPersistable
////    {
////        public new event Act onUpdate = delegate{  };
////
////        public Store(): base(){
////            base.onUpdate += invalidateStorages;
////        }
////
////        private void invalidateStorages(){
////
////        }
////    }
//    
////    /// <summary>
////    /// An entry-point Storage.
////    /// </summary>
////    public class Storage<T> : IPersistable
////    {
////        /// <summary>
////        /// The actual storage
////        /// </summary>
////        private readonly IDictionary<string, T> _storage;
////
//////        public IDictionary<string, T> storage
//////        {
//////            get { return _storage; }
//////        }
////
////        /// <summary>
////        /// Invoked when storage updates
////        /// string key is the key that was changed
////        /// </summary>
////        public event Act onUpdate = delegate{ };
////
////        /// <summary>
////        /// Initialize the storage map
////        /// </summary>
////        public Storage(){
////            _storage = new Dictionary<string, T>();
////        }
////
////        /// <summary>
////        /// Updates the storage map
////        /// by merging the data map into storage map
////        /// </summary>
////        public void Merge(IDictionary<string, T> data){
////            foreach (KeyValuePair<string, T> pair in data)
////                Set(pair.Key, pair.Value, false);
////        }
////
////        /// <summary>
////        /// Clears storage and sets the data
////        /// </summary>
////        /// <param name="data"></param>
////        public void SetAll(IDictionary<string, T> data){
////            _storage.Clear();
////            foreach (KeyValuePair<string, T> pair in data)
////                Set(pair.Key, pair.Value, false);
////        }
////
////        /// <summary>
////        /// Sets value for key
////        /// </summary>
////        public void Set(string key, T value){
////            Set(key, value, true);
////        }
////
////        /// <summary>
////        /// Internal's set
////        /// Allows to specify whether we
////        /// should notify an event or not
////        /// </summary>
////        /// <param name="key"></param>
////        /// <param name="value"></param>
////        /// <param name="notifyEvent"></param>
////        internal void Set(string key, T value, bool notifyEvent){
////            bool exists = _storage.ContainsKey(key);
////
////            if (exists)
////                _storage[key] = value;
////            else
////                _storage.Add(key, value);
////
////            if (notifyEvent)
////                onUpdate();
////        }
////
////        /// <summary>
////        /// Gets an item from the storage
////        /// </summary>
////        public T Get(string id, T notFoundValue = default(T), bool notFoundCreate = false){
////            T value;
////            if (_storage.TryGetValue(id, out value))
////                return value;
////
////            // Value is not found, automatically create it if notFoundCreate = true
////            if (notFoundCreate)
////                Set(id, notFoundValue);
////
////            return notFoundValue;
////        }
////        
////        public IDictionary<string, string> getDictionary(){
////            IDictionary<string, string> dict = new Dictionary<string, string>();
////            foreach (KeyValuePair<string,T> pair in _storage){
////                dict.Add(pair.Key, pair.Value.ToString());
////            }
////            return dict;
////        }
////
////        public void load(IDictionary<string, string> loadObject){
////            IDictionary<string, T> dict = new Dictionary<string, T>();
////            foreach (KeyValuePair<string, string> pair in loadObject){
////                dict.Add(pair.Key, GetValue<T>(pair.Value));
////            }
////            
////            SetAll(dict);
////        }
////        
////        public static TA GetValue<TA>(string value)
////        {
////            return (TA)Convert.ChangeType(value, typeof(T));
////        }
////    }
////    
//}


using UnityEngine;

namespace Snowglobe.Core
{
    public class Storage<T>
    {
        public event Act onUpdate = delegate { };
        private string _storageKey;
        private PersistedFile _file;

        public Storage(PersistedFile file, string storageKey)
        {
            _storageKey = storageKey;
            _file = file;
        }

        public string GetFileKey(string key)
        {
            return string.Format("{0}-{1}", _storageKey, key);
        }

        public void Set(string key, T value)
        {
            Set(key, value, true);
        }

        internal void Set(string key, T value, bool notifyEvent)
        {
            string fileKey = GetFileKey(key);
            _file.SetValue(fileKey, value);

            if (notifyEvent)
                onUpdate();
        }

        /// <summary>
        /// Gets an item from the storage
        /// </summary>
        public T Get(string key, T defaultValue = default(T), bool notFoundCreate = false)
        {
            string fileKey = GetFileKey(key);
            
            if (_file.HasKey(fileKey))
                return _file.getValue(fileKey, defaultValue);

            if (notFoundCreate)
                _file.SetValue(fileKey, defaultValue);

            return defaultValue;
        }
    }

    public static class StorageExtensions
    {
        public static int GetInt(this Storage<float> storage, string key)
        {
            return (int) storage.Get(key);
        }

        public static long GetLong(this Storage<float> storage, string key)
        {
            return (long) storage.Get(key);
        }


        public static int Increase(this Storage<float> storage, string key, int amount)
        {
            float val = storage.Get(key);
            val += amount;
            storage.Set(key, val);
            return (int) (val + amount);
        }

        public static void Decrease(this Storage<float> storage, string key, int amount)
        {
            float val = storage.Get(key);
            val -= amount;
            storage.Set(key, val);
        }

        public static int Increase(this Storage<int> storage, string key, int amount)
        {
            int val = storage.Get(key);
            val += amount;
            storage.Set(key, val);
            return (int) (val + amount);
        }

        public static void Decrease(this Storage<int> storage, string key, int amount)
        {
            int val = storage.Get(key);
            val -= amount;
            storage.Set(key, val);
        }

        public static bool MoreThan(this Storage<float> storage, string key, int target)
        {
            float val = storage.Get(key);
            return val >= target;
        }

        public static bool LessThan(this Storage<float> storage, string key, int target)
        {
            float val = storage.Get(key);
            return val <= target;
        }

        public static bool Has(this Storage<bool> storage, string key)
        {
            return storage.Get(key);
        }

        public static void Unlock(this Storage<bool> storage, string key)
        {
            storage.Set(key, true);
        }
    }
}