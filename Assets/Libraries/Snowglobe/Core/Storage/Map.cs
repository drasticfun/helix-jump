﻿using System.Collections;

namespace Snowglobe.Core.Storage
{
    public class Map : Hashtable
    {
        public event Act onUpdate = delegate(){ };

        public void on(string key){
            if (this[key] != null)
                this[key] = true;
            else
                Add(key, true);

            onUpdate();
        }

        public void off(string key){
            if (this[key] != null)
                this[key] = false;
            else
                Add(key, false);

            onUpdate();
        }

        public void put(string key, object value){
            if (this[key] != null)
                this[key] = value;
            else
                Add(key, value);

            onUpdate();
        }

        public void putAll(Map map){
            foreach (DictionaryEntry entry in map)
                this[entry.Key] = map[entry.Key];

            onUpdate();
        }

        public T get<T>(string key, T defaultValue = default(T)){
            var value = this[key];

            if (value is T)
                return (T) value;

            return defaultValue;
        }

        public bool has(string key, bool def = false){
            bool cur = get(key, def);
            
            if (!cur && ContainsKey(key) && !(this[key] is bool)){
                return true;
            }

            return cur;
        }

        public bool moreThan(string key, int num, bool equalIsMore = true){
            int cur = get(key, int.MinValue);
            return num > cur;
        }

        public bool lessThan(string key, int num, bool equalIsLess = true){
            int cur = get(key, int.MinValue);
            return num < cur;
        }

        public bool equals(string key, object target){
            var cur = this[key];
            return cur != null && cur.Equals(target);
        }
    }
}