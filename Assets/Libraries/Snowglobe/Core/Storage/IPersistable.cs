﻿using System.Collections.Generic;

namespace Snowglobe.Core.Storage
{
    public interface IPersistable
    {
        /// <summary>
        /// Will occur on loads
        /// </summary>
        void load(IDictionary<string, string> loadObject);
        
        /// <summary>
        /// Persistor will ask for the dictionary that it needs to save
        /// </summary>
        /// <returns></returns>
        IDictionary<string, string> getDictionary();
        
        /// <summary>
        /// Implementors should invoke this event
        /// when they require an update
        /// </summary>
        event Act onUpdate;
    }
}