﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.RemoteConfig;
using System;
using System.Threading.Tasks;
using Snowglobe.Core.Storage;


namespace Snowglobe.Core
{
    /// <summary>
    /// Handy class for handling a Config object
    /// with remotely updated variables using
    /// RemoteSettings
    /// 
    /// Config needs to be initialized with RemoteVar list
    /// 
    /// RemoteVar:
    ///     Each RemoteVar contains a default variable, a type and a name
    /// 
    /// On Initialization, Config will invalidate itself against RemoteSettings.
    /// At the same time, Config will listen to the RemoteSettings.Updated event,
    /// and will invalidate upon it.
    /// </summary>
    public class Config : Map
    { 
        [Serializable]
        public class RemoteVar
        {
            public enum VarType
            {
                Bool,
                Int,
                Float,
                String
            }

            public string Name;
            public string DefaultValue;
            public VarType Type;
        }

        private RemoteVar[] _remoteVars;

        /// <summary>
        /// Initializes Config
        /// Listens to RemoteSettings
        /// updates and init default
        /// values by using invalidateRemote() 
        /// </summary>
        public Config(RemoteVar[] remoteVars) : base()
        {
            if (remoteVars != null)
            {
                Debug.Log("Config() Init");
                _remoteVars = remoteVars;
                setDefaults();
                fetchRemote();
            }
        }

        private void setDefaults()
        {
            Map map = new Map();
            Dictionary<string, object> defaults =
                new Dictionary<string, object>();

            Debug.Log("Config: Setting Defaults");
            foreach (RemoteVar remoteVar in _remoteVars)
            {
                try
                {
                    string defStr = remoteVar.DefaultValue;
                    switch (remoteVar.Type)
                    {
                        case RemoteVar.VarType.Bool:
                            bool defBool = bool.Parse(defStr);
                            map.put(remoteVar.Name, defBool);
                            defaults.Add(remoteVar.Name, defBool);
                            break;
                        case RemoteVar.VarType.Int:
                            int defInt = int.Parse(defStr);
                            map.put(remoteVar.Name, defInt);
                            defaults.Add(remoteVar.Name, defInt);
                            break;
                        case RemoteVar.VarType.Float:
                            float defFloat = float.Parse(defStr);
                            map.put(remoteVar.Name, defFloat);
                            defaults.Add(remoteVar.Name, defFloat);
                            break;
                        case RemoteVar.VarType.String:
                            map.put(remoteVar.Name, defStr);
                            defaults.Add(remoteVar.Name, defStr);
                            break;
                    }
                    
                    Debug.Log(string.Format("Config: Setting Default Value For: {0} / {1} as {2}", remoteVar.Name, defStr, remoteVar.Type.ToString()));
                }
                catch
                {
                    Debug.Log("Config: Error Setting Defaults");
                }
            }

            Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
            
            foreach (RemoteVar remoteVar in _remoteVars)
            {
                try
                {
                    string defStr = FirebaseRemoteConfig.GetValue(remoteVar.Name).StringValue;
                    switch (remoteVar.Type)
                    {
                        case RemoteVar.VarType.Bool:
                            bool defBool = bool.Parse(defStr);
                            map.put(remoteVar.Name, defBool);
                            break;
                        case RemoteVar.VarType.Int:
                            int defInt = int.Parse(defStr);
                            map.put(remoteVar.Name, defInt);
                            break;
                        case RemoteVar.VarType.Float:
                            float defFloat = float.Parse(defStr);
                            map.put(remoteVar.Name, defFloat);
                            break;
                        case RemoteVar.VarType.String:
                            map.put(remoteVar.Name, defStr);
                            break;
                    }
                    
                    Debug.Log(string.Format("Config: Setting Initial Value For: {0} / {1} as {2}", remoteVar.Name, defStr, remoteVar.Type.ToString()));
                }
                catch
                {
                    Debug.Log("Config: Error Setting Initial RemoteVar");
                }
            }
            // Merge in new config variables
            putAll(map);
        }

        private void fetchRemote()
        {
#if UNITY_EDITOR
            return;
#else
Task fetchTask = FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
            fetchTask.ContinueWith(FetchComplete);
#endif
        }

        void FetchComplete(Task fetchTask)
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("FirebaseRemoteConfig: Fetch canceled");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("FirebaseRemoteConfig: Fetch Faulted");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("FirebaseRemoteConfig: Fetch completed successfully!");
            }

            var info = FirebaseRemoteConfig.Info;

            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    Debug.Log(string.Format("FireBaseRemoteConfig: Fetch Success{0}", FirebaseRemoteConfig.ActivateFetched()));

                    Map map = new Map();
                    foreach (RemoteVar remoteVar in _remoteVars)
                    {
                        try
                        {
                            Debug.Log(string.Format("FireBaseConfig: Getting value: {0}", remoteVar.Name));
                            Debug.Log(string.Format("FireBaseConfig: Current Value: {0}", FirebaseRemoteConfig.GetValue(remoteVar.Name)));
                            ConfigValue firebaseConfig = FirebaseRemoteConfig.GetValue(remoteVar.Name);
                            Debug.Log(string.Format("FireBaseConfig: New Value: {0}", firebaseConfig.StringValue));
                            switch (remoteVar.Type)
                            {
                                case RemoteVar.VarType.Bool:
                                    map.put(remoteVar.Name, firebaseConfig.BooleanValue);
                                    break;
                                case RemoteVar.VarType.Int:
                                    Debug.Log(remoteVar.Name);
                                    Debug.Log((int)firebaseConfig.LongValue);
                                    map.put(remoteVar.Name,
                                        (int) firebaseConfig.LongValue);
                                    break;
                                case RemoteVar.VarType.Float:
                                    map.put(remoteVar.Name,
                                        (float) firebaseConfig.DoubleValue);
                                    break;
                                case RemoteVar.VarType.String:
                                    map.put(remoteVar.Name, firebaseConfig.StringValue);
                                    break;
                            }
                            Debug.Log(string.Format("Config: Setting Remote Value For: {0} / {1} as {2}", remoteVar.Name, firebaseConfig.StringValue, remoteVar.Type.ToString()));
                            
                        }
                        catch 
                        {
                            Debug.Log("Exception in remote config");
                            // ignored
                        }
                    }

                    putAll(map);
                    Debug.Log("Config ----Logging New Map-----");
                    foreach (DictionaryEntry entry in this)
                    {
                        Debug.Log(string.Format("Key:{0} Value:{1}", entry.Key, entry.Value));
                    }
                    Debug.Log("Config ---------");
                    break;
                case LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case FetchFailureReason.Error:
                            Debug.Log("FireBaseRemoteConfig: Fetch failed for unknown reason");
                            break;
                        case FetchFailureReason.Throttled:
                            Debug.Log("FireBaseRemoteConfig: " + info.ThrottledEndTime);
                            break;
                    }

                    break;
                case LastFetchStatus.Pending:
                    Debug.Log("FireBaseRemoteConfig: Latest Fetch call still pending.");
                    break;
            }
        }
    }
}