﻿using System.Collections.Generic;
using UnityEngine;

namespace Snowglobe.Core
{
    public abstract class Service : Object<App>
    {
        /// <summary>
        /// Called only once in the initialization phase
        /// Should return a unique service.
        /// By default it uses the class name
        /// </summary>
        public virtual string getServiceName(){
            return GetType().Name;
        }
    }
}