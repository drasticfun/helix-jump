﻿using System;
using System.Collections;
using System.Collections.Generic;
using Snowglobe.Core.Storage;
using Snowglobe.Utils;
using Snowglobe.Virtuals;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Snowglobe.Core
{
    // @TODO: Make sure you initialize all services before starting the app.
    [DefaultExecutionOrder(-100)]
    public abstract class App : Object<App>
    {
        #region Events

        public event Act<Map> OnLoading = delegate(Map instance) {  };

        public event Act<Map> OnStart = delegate(Map instance) { };

        #endregion

        #region Configuration

        public Config Config { get; private set; }

        [SerializeField] private AppSettings _settings;

        public AppSettings Settings
        {
            get { return _settings; }
        }

        #endregion

        #region App States/Instance/Stats

        public class States
        {
            public static readonly string Initialized = "initialized";
            public static readonly string Loading = "loading";
            public static readonly string Started = "started";
        }

        public class AppInstance
        {
            public const string IsFirstLaunch = "is_first_launch";
            public const string IsDayFirstLaunch = "app_first_launch_day";
        }

        public class AppToggles
        {
            public const string Launched = "app_launched";
        }

        public class AppStats
        {
            public static readonly string ConsecutiveDays = "consecutive_days";
            public static readonly string UniqueDailyLogins = "unique_daily_logins";
            public const string LastDailyTime = "last_daily_time";
        }

        public Map Instance { get; private set; }

        #endregion

         #region Persistance

        public Preferences Prefs { get; private set; }

        public PersistedFile AppSave { get; private set; }
        
        public Storage<float> Stats { get; private set; }

        public Storage<float> Currencies { get; private set; }

        public Storage<string> Slots { get; private set; }

        public Storage<bool> Toggles { get; private set; }

        public Storage<bool> Unlocks { get; private set; }

        public Storage<bool> CompletedMissions { get; private set; }

        #endregion

        #region Virtuals

        public VirtualsPool Pool { get; private set; }

        #endregion

        public Services Services { get; private set; }

        #region Initialization

        protected override void preInitialize()
        {
            Instance = new Map();
            Application.targetFrameRate = 60;

            InitPersistance();
            InitConfig();
            InitVirtuals();
            InitServices();
            CheckFirstLaunch();
            CheckDaily();
            
            Game.Game game = getChild<Game.Game>();
            if(game != null)
                game.Deactivate();  
           
        }

        protected override void initialize()
        {
            Instance.on(States.Initialized);
            LoadApp();
        }

        private void InitPersistance()
        {
            // Init Preferences
            Prefs = new Preferences(GetType().FullName);
            AppSave = new PersistedFile("app_save");
            // Load persisted storages
            Stats = new Storage<float>(AppSave, "app_stats");
            Currencies = new Storage<float>(AppSave, "app_currencies");
            Slots = new Storage<string>(AppSave, "app_slots");
            Toggles = new Storage<bool>(AppSave, "app_toggles");
            Unlocks = new Storage<bool>(AppSave, "app_unlocks");
            CompletedMissions = new Storage<bool>(AppSave, "app_completed_missions");
        }

        private void InitConfig()
        {
            Config = new Config(_settings.RemoteConfigVars);
        }

        private void InitVirtuals()
        {
            IList<Virtual> pool = new List<Virtual>();
            foreach (string folder in Settings.VirtualsFolders)
            {
                IList<Virtual> list = Resources.LoadAll<Virtual>(string.Format("Virtuals/{0}", folder));
                foreach (Virtual virt in list)
                {
                    pool.Add(virt);
                }
            }

            Pool = new VirtualsPool(pool);
        }

        private void InitServices()
        {
            Services = getChild<Services>();
            if (Services != null)
                Services.Activate();
        }

        private void CheckFirstLaunch()
        {
            bool launched = Toggles.Get(AppToggles.Launched, false);
            if (!launched)
            {
                Instance.put(AppInstance.IsFirstLaunch, true);
                Toggles.Set(AppToggles.Launched, true);
            }
        }

        private void CheckDaily()
        {
            DateTime startOfDay = TimeHelp.StartOfDay;

            long lastDailyTime = (long) Stats.Get(AppStats.LastDailyTime, -1);
            if (lastDailyTime == -1 || !Toggles.Has(AppToggles.Launched))
            {
                lastDailyTime = startOfDay.Ticks;
                Stats.Set(AppStats.LastDailyTime, startOfDay.Ticks);
                Stats.Set(AppStats.ConsecutiveDays, 1);
                Stats.Increase(AppStats.UniqueDailyLogins, 1);
            }

            TimeSpan timeSinceLastDailyTime = TimeHelp.GetTimeSince(lastDailyTime);

            if (timeSinceLastDailyTime.Days >= 1)
            {
                Instance.put(AppInstance.IsDayFirstLaunch, true);
                Stats.Set(AppStats.LastDailyTime, startOfDay.Ticks);
                Stats.Increase(AppStats.UniqueDailyLogins, 1);
                if (timeSinceLastDailyTime.Days < 2)
                    Stats.Increase(AppStats.ConsecutiveDays, 1);
                else
                    Stats.Set(AppStats.ConsecutiveDays, 1);
            }
        }

        #endregion

        #region App Lifecycle    

        private void LoadApp()
        {
            log("Loading");

            Game.Game game = getChild<Game.Game>();
            if (game)
                game.Deactivate();
            
            Loading loadingScreen = getChild<Loading>();
            if (loadingScreen)
                loadingScreen.Activate();

            Services services = getChild<Services>();
            if (services)
                services.Activate();

            Instance.on(States.Loading);
            OnLoading(Instance);

            if (Settings.IsDebug)
                Invoke("Loaded", 2.0f);
            else
                StartCoroutine(LoadCo());
        }

        private IEnumerator LoadCo()
        {
            float loadStartTime = Time.time;
            
            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
            sceneLoad.allowSceneActivation = false;
            while (sceneLoad.progress < 0.9f)
            {
                log(string.Format("{0}: Loading scene..", sceneLoad.progress));
                yield return null;
            }

            sceneLoad.allowSceneActivation = true;
            yield return new WaitForSeconds(0.1f);
            Scene scene = SceneManager.GetSceneAt(1);
            GameObject[] rootObjs = scene.GetRootGameObjects();
            foreach (GameObject rootObj in rootObjs)
            {
                Game.Game game = rootObj.GetComponent<Game.Game>();
                if (game)
                {
                    game.transform.parent = transform;
                    game.transform.SetAsLastSibling();
                    
                }
                else
                    rootObj.transform.parent = transform.root;
            }
            yield return new WaitForSeconds(0.5f);

            // Minimum 3 seconds loading time
//            float timeDiff = Time.time - loadStartTime;
//            if (timeDiff <= 3)
//            {
//                yield return new WaitForSeconds(3 - timeDiff);
//            }

            SceneManager.UnloadSceneAsync(scene);
            Loaded();
        }

        private void Loaded()
        {
            log("Loaded");

            Instance.off(States.Loading);
            start();
        }

        // ReSharper disable once InconsistentNaming
        private void start()
        {
            if (Instance.has(States.Initialized) && !Instance.has(States.Loading))
            {
                log("Starting");

                Loading loading = getChild<Loading>();
                if (loading)
                    Destroy(loading.gameObject);

                getChild<Game.Game>().Activate();

                Instance.on(States.Started);
                StartApp();
                OnStart(Instance);
            }
            else
                log("StartApp() - Not Started App is not Initialized");
        }

        protected abstract void StartApp();

        #endregion
    }
}