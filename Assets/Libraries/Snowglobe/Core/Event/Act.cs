﻿namespace Snowglobe.Core
{
    public delegate void Act();
    public delegate void Act<T>(T arg);
}