﻿using System;
using PaperPlaneTools;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Snowglobe.Core
{
    [Serializable]
    [CreateAssetMenu(fileName = "Settings", menuName = "Snow/App/Settings", order = 1)]
    public class AppSettings : ScriptableObject
    {
        #region AppConfig

        [Header("App Config")] [SerializeField]
        internal string[] VirtualsFolders;

        [SerializeField] internal Config.RemoteVar[] RemoteConfigVars;

        #endregion

        #region Store

        [Header("Store")] 
        [SerializeField] private string _googleBundleId;
        [SerializeField] private string _appleAppId;

        public string GoogleBundleId
        {
            get { return _googleBundleId; }
        }
        
        public string AppleAppId
        {
            get { return _appleAppId; }
        }

        public string StoreLink
        {
            get
            {
                string url = RateBox.GetStoreUrl(_appleAppId, _googleBundleId);
                return url;
            }
        }

        public string GoogleStoreLink
        {
            get
            {
                string url = String.Format("https://play.google.com/store/apps/details?id={0}",  WWW.EscapeURL(_googleBundleId));
                return url;
            }
        }
        
        public string AppleStoreLink
        {
            get
            {
                string url = String.Format("https://itunes.apple.com/app/id{0}",  WWW.EscapeURL(_appleAppId));
                return url;
            }
        }
        #endregion

        #region Monetization

        [SerializeField] private string _androidIsAppKey = "";
        [SerializeField] private string _iosIsAppKey = "";

        // ReSharper disable once InconsistentNaming
        public string ISAppKey
        {
            get
            {
                if (Application.platform == RuntimePlatform.Android)
                    return _androidIsAppKey;
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                    return _iosIsAppKey;
                return "";
            }
        }

        #endregion

        #region Leaderboards

        [Header("Leaderboards")] 
        [SerializeField]
        private string _androidLeaderboardsId = "";

        [SerializeField] 
        private string _iosLeaderboardsId = "";

        // ReSharper disable once InconsistentNaming
        public string LeaderboardsId
        {
            get
            {
                if (Application.platform == RuntimePlatform.Android)
                    return _androidLeaderboardsId;
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                    return _iosLeaderboardsId;
                return "";
            }
        }

        public string TenjinApiKey = "";

        #endregion

        [Header("Products")]
        [SerializeField]
        public Product[] Products;

        [Serializable]
        public class Product
        {
            [SerializeField]
            private string _id;
            [SerializeField]
            private ProductType _type;

            public string Id
            {
                get { return _id; }
                set { _id = value; }
            }
            
            public ProductType Type
            {
                get { return _type; }
            }
        }
        
        #region Debug

        [Header("Debug")] [SerializeField] internal bool IsDebug;

        #endregion
    }
}