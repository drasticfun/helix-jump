﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Snowglobe.Core
{
    public class Services : Object<App>
    {
        private IDictionary<string, Service> _services = new Dictionary<string, Service>();

        protected override void preInitialize(){
            base.preInitialize();

            for (int i = 0; i < transform.childCount; i++){
                Transform child = transform.GetChild(i);

                Service childService = child.GetComponent<Service>();
                if (childService != null){
                    if (add(childService)){
                        childService.gameObject.SetActive(true);
                        log(string.Format("Added: {0}", childService.getServiceName()));
                    }
                    else{
                        // Conflict service
                        log(string.Format("Invalid service(Conflict) for gameObject: {0}", child.name));
                        child.gameObject.SetActive(false);
                    }
                }
                else{
                    // Invalid child service
                    // Disable object
                    log(string.Format("Invalid service(NonService) for gameObject: {0}", child.name));
                    child.gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Adds a service to app
        /// </summary>
        /// <returns></returns>
        public bool add(Service service){
            // The service name
            String serviceName = service.getServiceName();

            // Couldn't add the service because a
            // service with the same name exists
            if (_services.ContainsKey(serviceName))
                return false;

            // Add the service to our dictionary
            _services.Add(serviceName, service);
            
            return true;
        }

        /// <summary>
        /// Finds and returns the first service
        /// of type T  
        /// </summary>
        /// <returns></returns>
        public T Get<T>() where T : Service{
            // Iterate our services
            foreach (KeyValuePair<string, Service> pair in _services){
                // Check if child is of type T
                if (pair.Value.GetType() == typeof(T))
                    // Found service of type T
                    return (T) pair.Value;
            }

            // Did not find a matching service
            return null;
        }

        /// <summary>
        /// Finds and return service
        /// with the name of serviceName
        /// </summary>
        /// <returns></returns>
        public T Get<T>(string serviceName) where T : Service{
            Service service;
            if (_services.TryGetValue(serviceName, out service)){
                Service castedService = service as T;
                if (castedService != null)
                    return (T) castedService;
            }

            // Did not find a matching service
            return null;
        }
    }
}