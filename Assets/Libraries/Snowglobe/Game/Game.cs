﻿using System.Collections.Generic;
using Snowglobe.Core;
using Snowglobe.Core.Storage;
using Snowglobe.GameHooks;
using Debug = UnityEngine.Debug;

namespace Snowglobe.Game
{
    public abstract class Game : Object<App>
    {
        public abstract event Act<Session> OnSessionInit;
        private readonly List<GameHook> _gameHooks = new List<GameHook>();

        protected override void preInitialize()
        {
            RegisterDefaultHooks();
        }

        #region Game Hooks

        private void RegisterDefaultHooks()
        {
            RegisterHook(new SimpleInterstitial(this));
            RegisterHook(new SimpleBanner(this));
            RegisterHook(new SimpleAnalytics(this));
        }

        protected void RegisterHook(GameHook hook)
        {
            _gameHooks.Add(hook);
        }

        protected T GetHook<T>(GameHook hook) where T : GameHook
        {
            foreach (var gameHook in _gameHooks)
            {
                if (gameHook is T)
                    return gameHook as T;
            }

            return null;
        }

        #endregion
    }
}