﻿using System;
using Snowglobe.Core;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Snowglobe.Services
{
    public class Purchases : Service, IStoreListener
    {
        public interface IProductsHandler
        {
            bool HandleProduct(string productId);
        }

        private static IStoreController _storeController;
        private static IExtensionProvider _storeProvider;

        public event Act OnInitialize = delegate {  };
        
        #region Lifecycle

        protected override void initialize()
        {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            foreach (var product in App.Settings.Products)
            {
                builder.AddProduct(product.Id, product.Type);
                log(string.Format("Adding {0} Product: {1}", product.Id, product.Type.ToString()));
            }

            UnityPurchasing.Initialize(this, builder);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            log("Init Success");
            _storeController = controller;
            _storeProvider = extensions;
            OnInitialize();
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            log(string.Format("Init Failed: {0}", error));
        }

        private bool isStoreInit()
        {
            return _storeController != null && _storeProvider != null;
        }

        #endregion

        #region API

        public void Buy(string productId)
        {
            if (isStoreInit())
            {
                Product product = _storeController.products.WithID(productId);
                if (product != null && product.availableToPurchase)
                {
                    log(string.Format("Purchasing: '{0}'", product.definition.id));
                    _storeController.InitiatePurchase(product);
                }
            }
        }

        public void Restore()
        {
            if (isStoreInit())
            {
                if (Application.platform == RuntimePlatform.IPhonePlayer ||
                    Application.platform == RuntimePlatform.OSXPlayer)
                {
                    var apple = _storeProvider.GetExtension<IAppleExtensions>();
                    apple.RestoreTransactions((result) => { log(string.Format("Restore: {0}", result)); });
                }
            }
        }

        public double GetPrice(string productId)
        {
            if (isStoreInit())
            {
                if (_storeController.products != null)
                {
                    Product product = _storeController.products.WithID(productId);
                    if (product != null && product.availableToPurchase)
                        if (product.metadata != null)
                            return (double) product.metadata.localizedPrice;
                }
            }

            return -1;
        }

        public string GetPriceText(string productId)
        {
            if (isStoreInit())
            {
                if (_storeController.products != null)
                {
                    Product product = _storeController.products.WithID(productId);
                    if (product != null && product.availableToPurchase)
                        if (product.metadata != null)
                            return product.metadata.localizedPriceString;
                }
            }

            return "";
        }

        #endregion

        #region Purchase Handling

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            bool handle = false;
            if (App is IProductsHandler)
            {
                IProductsHandler handler = App as IProductsHandler;
                try
                {
                    log("Handling product");
                    handle = handler.HandleProduct(args.purchasedProduct.definition.id);
                }
                catch
                {
                    // ignored
                    log(string.Format("onPurchase() handle exception"));
                }
            }

            if (!handle)
                log(string.Format("onPurchase() unknown product: ", args.purchasedProduct.definition.id));


            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            log(string.Format("onPurchaseFailed(): {0}", failureReason));
        }

        #endregion
    }
}