﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;
using TapticPlugin;
using UnityEngine;

namespace Snowglobe.Services
{
    public class Haptics : Service
    {
        public class Settings
        {
            public const string Enabled = "haptics_enabled";
        }

        private Ads _ads;
        private bool _muted = false;
        private bool _hapSupported = false;

        protected override void initialize()
        {
            _ads = App.Services.Get<Ads>();

            _hapSupported = Application.platform == RuntimePlatform.IPhonePlayer;
            App.OnStart += AppStart;
        }

        private void AppStart(Map arg)
        {
            if (App.Toggles.Has(App.AppToggles.Launched))
                App.Prefs.Set(Settings.Enabled, true);

            RefreshMuted();

            ListenToAdsEvents();
        }

        #region API

        public void Hap(ImpactFeedback feedback = ImpactFeedback.Light)
        {
            if (!_muted && _hapSupported)
                TapticManager.Impact(feedback);
        }

        public void Vibrate()
        {
            if (!_muted)
                Handheld.Vibrate();
        }

        #endregion
        
        private void ListenToAdsEvents()
        {
            _ads.Video.onOpen += Mute;
            _ads.Video.onClose += RefreshMuted;
            _ads.Interstitial.onOpen += Mute;
            _ads.Interstitial.onClose += RefreshMuted;
        }

        private void RefreshMuted()
        {
            _muted = !App.Prefs.Get(Settings.Enabled);
        }

        private void Mute()
        {
            _muted = true;
        }

        private void Unmute()
        {
            _muted = false;
        }
    }
}