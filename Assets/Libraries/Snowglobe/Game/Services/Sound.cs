﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;
using UnityEngine;

namespace Snowglobe.Services
{
    /// <summary>
    /// A sound effect service
    /// Lets you play audioclips,
    /// with options for volume and pitch
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class Sound : Service
    {
        private Ads _ads;
        
        /// <summary>
        /// Default play options ( volume, pitch ) 
        /// </summary>
        [SerializeField] private PlayOptions
            _playOptions = new PlayOptions(1f, -1);

        private bool _muted = false;
        
        private AudioSource _audioSource;
        
        public void play(AudioClip clip, float volume)
        {
            play(clip, new PlayOptions(volume, -1f));
        }

        public void play(AudioClip clip, float volume, float pitch)
        {
            play(clip, new PlayOptions(volume, pitch));
        }
        
        public void play(AudioClip clip, PlayOptions options)
        {
            // Stop execution if we are currently muted
            if (_muted)
                return;
            
            // Pitch
            if (options.Pitch != -1)
                _audioSource.pitch = options.Pitch;

            // Plays the clip with the volume supplied
            _audioSource.PlayOneShot(clip, options.Volume);
        }

        public void play(AudioClip clip)
        {
            play(clip, _playOptions);
        }

        protected override void initialize()
        {   
            _audioSource = GetComponent<AudioSource>();
            App.OnStart += AppStarted;
        }
        
        private void AppStarted(Map instance){
            _ads = App.Services.Get<Ads>();
            _ads.Video.onOpen += mute;
            _ads.Video.onClose += refreshMuted;

            _ads.Interstitial.onOpen += mute;
            _ads.Interstitial.onClose += refreshMuted;
            
            App.Prefs.OnUpdate += refreshMuted;
            refreshMuted();
        }

        private void mute()
        {
            _muted = true;
        }

        // Invalidates _muted based on app.settings
        private void refreshMuted()
        {
            // Find if we are muted with app settings
            _muted = !App.Prefs.Get(SoundService.Enabled);
        }
    }

    public class SoundService
    {
        public const string Enabled = "sfx_enabled";
    }
    
    
    public class PlayOptions
    {
        public float Volume;
        public float Pitch;

        public PlayOptions(float volume, float pitch)
        {
            Volume = volume;
            Pitch = pitch;
        }
    }
}