﻿using System.Collections.Generic;
using Firebase.Analytics;
using Snowglobe.Core;
using Snowglobe.Core.Storage;

namespace Snowglobe.Services
{
    public class Tracker: Service
    {
        private bool _didActivate = false;
        
        protected override void initialize()
        {
            ActivateTenjin();
        }
        
        private void OnApplicationPause(bool paused)
        {
            if(!paused && _didActivate)
                ActivateTenjin();
        }

        private void ActivateTenjin()
        {
            BaseTenjin tenjin = Tenjin.getInstance(App.Settings.TenjinApiKey);
            tenjin.Connect();
            tenjin.GetDeeplink(HandleTenjinDeepLink);
            _didActivate = true;
        }

        private void HandleTenjinDeepLink(Dictionary<string, string> data)
        {
            
            if (data.ContainsKey("ad_network"))
                FirebaseAnalytics.SetUserProperty("tenjin_ad_network", data["ad_network"]);
            if (data.ContainsKey("campaign_id"))
                FirebaseAnalytics.SetUserProperty("tenjin_campaign_id", data["campaign_id"]);
            if (data.ContainsKey("is_first_session"))
                FirebaseAnalytics.SetUserProperty("tenjin_is_first_session", data["is_first_session"]);
            if (data.ContainsKey("deferred_deeplink_url"))
                FirebaseAnalytics.SetUserProperty("tenjin_deferred_deeplink_url", data["deferred_deeplink_url"]);
        }
    }
}