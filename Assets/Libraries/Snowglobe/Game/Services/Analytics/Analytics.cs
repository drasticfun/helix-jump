﻿using System.Collections.Generic;
using Code.Session;
using Facebook.Unity;
using Firebase.Analytics;
//using Firebase.Analytics;
using Snowglobe.Core;
using Snowglobe.Core.Storage;
using UnityEngine;
using UnityEngine.Analytics;
    
namespace Snowglobe.Services
{
    public class Analytics: Service
    {
        protected override void preInitialize()
        {
            base.preInitialize();
            ActivateFB();
        }

        private void OnApplicationPause(bool paused)
        {
            if (!paused)
                ActivateFB();
        }
        
        // Inits / Activates Facebook SDK
        private void ActivateFB()
        {
            if (FB.IsInitialized)
                FB.ActivateApp();
            else
                FB.Init(() => { FB.ActivateApp(); });   
        }

        public void logEvent(string name, Dictionary<string, object> payload = null)
        {
            if (payload == null)
            {
//                UnityEngine.Analytics.Analytics.CustomEvent(name);
                if(FB.IsInitialized)
                    FB.LogAppEvent(name);
                
                Firebase.Analytics.FirebaseAnalytics.LogEvent(name);
            }
            else
            {
//                UnityEngine.Analytics.Analytics.CustomEvent(name, payload);
                
                if(FB.IsInitialized)
                    FB.LogAppEvent(name, null, payload);
                
                Parameter[] firebaseParams = new Parameter[payload.Count];
                int currIndex = 0;
                foreach (KeyValuePair<string,object> pair in payload)
                {
                    firebaseParams[currIndex] = new Parameter(pair.Key, pair.Value.ToString());
                    currIndex++;
                }
                FirebaseAnalytics.LogEvent(name, firebaseParams);
            }
        }
        
        public void logEvent(string name, string payloadKey, object payloadValue)
        { 
            Dictionary<string, object> payload = new Dictionary<string, object>();
            
            if(!string.IsNullOrEmpty(payloadKey) && payloadValue != null)
                payload.Add(payloadKey, payloadValue);
            
            logEvent(name, payload);
        }

        public void setProp(string key, string value)
        {
            FirebaseAnalytics.SetUserProperty(key, value);
        }
    }
}