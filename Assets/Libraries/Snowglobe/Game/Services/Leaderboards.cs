﻿using System;
using Snowglobe.Core;
using UnityEngine;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;

#endif

namespace Snowglobe.Services
{
    public class Leaderboards : Service
    {
        public class Settings
        
        {
            public const string LeaderboardId = "leaderboards_id";
        }

        private string _leaderboardsId;


        #region Lifecycle

        protected override void preInitialize()
        {
            Authenticate();
        }

        protected override void initialize()
        {
            _leaderboardsId = App.Settings.LeaderboardsId;

            if (string.IsNullOrEmpty(_leaderboardsId))
            {
                log("Missing Config: LeaderboardId");
            }
            else
                Configure();
        }

        #endregion

        #region Authentication

        private void Authenticate()
        {
            Authenticate(OnAuth);
        }

        private void Authenticate(Action<bool> act)
        {
            Social.localUser.Authenticate(act);
        }

        private void OnAuth(bool success)
        {
            log(string.Format("onAuth() success:{0}", success));
        }

        #endregion

        #region API

        public void Show()
        {
            if (Social.localUser.authenticated)
            {
                Social.ShowLeaderboardUI();
            }
            else
                Authenticate(success =>
                {
                    if (success) Show();
                });
        }

        public void Report(long score)
        {
            if (Social.localUser.authenticated)
            {
                Social.ReportScore(score, _leaderboardsId,
                    success => { log(string.Format("reportScore(): success:{0}", success)); });
            }
        }

        #endregion

        #region Android Config

        private void Configure()
        {
#if UNITY_ANDROID
            ConfigureAndroid();
#endif
        }

#if UNITY_ANDROID
        /// <summary>
        /// Enables GooglePlay Leaderboards
        /// </summary>
        private void ConfigureAndroid()
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                .Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = false;
            PlayGamesPlatform.Activate();
        }
#endif

        #endregion
    }
}