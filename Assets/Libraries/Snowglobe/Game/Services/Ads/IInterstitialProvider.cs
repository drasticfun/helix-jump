﻿using Snowglobe.Core;

namespace Snowglobe.Services
{
    public interface IInterstitialProvider
    {
        event Act onReady;
        event Act onOpen;
        event Act onClose;
        
        bool available();
        void show();
        void show(string placement);
        void load();
    }
}