﻿namespace Snowglobe.Services
{
    public interface IBannerProvider
    {
        void show();
        void hide();
        void load();
    }
}