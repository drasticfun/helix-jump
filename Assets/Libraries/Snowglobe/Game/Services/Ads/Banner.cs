﻿namespace Snowglobe.Services
{
    public class Banner: IBannerProvider
    {
        private bool _show = false;
        private bool _loaded = false;
        
        public Banner()
        {
            IronSourceEvents.onBannerAdLoadedEvent += BannerLoaded;
            IronSourceEvents.onBannerAdLoadFailedEvent += BannerLoadFailed;
            load();
        }
        
        public void load(){
            IronSource.Agent.loadBanner(IronSourceBannerSize.SMART_BANNER, IronSourceBannerPosition.BOTTOM);
        }
        
        private void BannerLoaded()
        {
            _loaded = true;
            if(_show)
                show();
        }
        
        private void BannerLoadFailed(IronSourceError err)
        {
            _loaded = false;
        }

        public void show()
        {
            _show = true;
            if(!_loaded)
                load();
            IronSource.Agent.displayBanner();
        }

        public void hide(){
            _show = false;
            IronSource.Agent.hideBanner();
        }
    }
}