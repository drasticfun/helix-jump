﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;

namespace Snowglobe.Services
{
    public class RewardedVideo : IRewardedVideoProvider
    {
        public event Act<bool> onAvailability = delegate(bool available){ };

        public event Act<string> onReward = delegate(string s){ };
        public event Act onOpen = delegate{ };
        public event Act onClose = delegate{ };
        
        // TODO: Implement
//        public event Act onCloseWithReward = delegate{ };
        
        public RewardedVideo(){
            IronSourceEvents.onRewardedVideoAdRewardedEvent += rewarded;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += availabilityChange;
            IronSourceEvents.onRewardedVideoAdOpenedEvent += opened;
            IronSourceEvents.onRewardedVideoAdClosedEvent += closed;
        }

        private void rewarded(IronSourcePlacement placement){
            onReward(placement.getPlacementName());
        }

        private void availabilityChange(bool available){
            onAvailability(available);
        }

        
        private void opened(){
            onOpen();
        }

        private void closed(){
            onClose();
        }

        
        public bool available(){
            return IronSource.Agent.isRewardedVideoAvailable();
        }

        public void show(){
            IronSource.Agent.showRewardedVideo();
        }

        public void show(string placement){
            IronSource.Agent.showRewardedVideo(placement);
        }
    }
}