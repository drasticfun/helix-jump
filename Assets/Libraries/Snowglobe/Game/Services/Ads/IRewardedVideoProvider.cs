﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;

namespace Snowglobe.Services
{
    public interface IRewardedVideoProvider
    {
        event Act<bool> onAvailability;
        event Act<string> onReward;
        event Act onOpen;
        event Act onClose;
        
        bool available();
        void show();
        void show(string placement);
    }
}