﻿using System;
using Snowglobe.Core;
using Snowglobe.Core.Storage;
using Snowglobe.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Snowglobe.Services
{
    /// <summary>
    /// A monetization service
    /// </summary>
    public class Ads : Service
    {
        public IBannerProvider Banner{ get; private set; }
        public IInterstitialProvider Interstitial{ get; private set; }
        public IRewardedVideoProvider Video{ get; private set; }
        
        protected override void initialize(){
            InitAds();
        }
        
        private void InitAds()
        {
            string appKey = App.Settings.ISAppKey;
            log(string.Format("InitAds: {0}", appKey));
            if (!string.IsNullOrEmpty(appKey)){
                IronSource.Agent.init(appKey);
                
                if (App.Settings.IsDebug)
                    IronSource.Agent.validateIntegration();
            }
            else
                log("Invalid Ads AppKey");
            
            Video = new RewardedVideo();
            
            log("loading ads--------");
            
            if (App.Toggles.Get(Toggles.NoAds)){
                DisableAds();
            }
            else{
                App.Toggles.onUpdate += CheckNoAds;
                
                Banner = new Banner();
                Interstitial = new Interstitial();
                
                if (App.Config.get<bool>(Config.IsOnLaunch)){
                    Interstitial.load();
                    Interstitial.onReady += OnInitialIsReady;
                }
            }

            addChild<IronSourceEvents>();
        }

        // Required by the IronSource SDK
        void OnApplicationPause(bool isPaused){
            IronSource.Agent.onApplicationPause(isPaused);
        }

        private void CheckNoAds(){
            bool hasNoAds = App.Toggles.Get(Toggles.NoAds);
            if (hasNoAds)
                DisableAds();
        }

        private void DisableAds(){
            if(Banner != null)
                Banner.hide();
            
            Banner = new MockBanner();
            Interstitial = new MockInterstitial();
        }

        private void OnInitialIsReady(){
            Interstitial.show();
            Interstitial.onReady -= OnInitialIsReady;
        }


        public class Toggles
        {
            public const string NoAds = "no_ads";
        }
        
        public class Config
        {
            public const string AppKey = "";
            public const string IsOnLaunch = "interstitial_on_launch";
        }
    }
}