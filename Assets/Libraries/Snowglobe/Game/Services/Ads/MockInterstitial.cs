﻿using Snowglobe.Core;
using UnityEngine;

namespace Snowglobe.Services
{
    // TODO: Log Analytics - Useful for checking no-ads ROI
    public class MockInterstitial: IInterstitialProvider
    {
        public event Act onReady = delegate
        {
            Debug.Log("MockInterstitial: Show");
        };
        public event Act onOpen = delegate
        {
            Debug.Log("MockInterstitial: Open");
        };
        
        public event Act onClose = delegate
        {
            Debug.Log("MockInterstitial: Close");
        };
        public bool available(){
            return false;
        }

        public void show(){
            
        }

        public void show(string placement){
            
        }

        public void load(){
            
        }
    }
}