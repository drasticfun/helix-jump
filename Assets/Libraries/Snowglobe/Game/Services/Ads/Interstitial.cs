﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;
using UnityEngine;

namespace Snowglobe.Services
{
    public class Interstitial : IInterstitialProvider
    {
        public event Act onReady = delegate{ };
        public event Act onOpen = delegate{ };
        public event Act onClose = delegate{ };

        private string _lastRequestedPlacement = null;

        public Interstitial(){
            IronSourceEvents.onInterstitialAdReadyEvent += ready;
            IronSourceEvents.onInterstitialAdOpenedEvent += opened;
            IronSourceEvents.onInterstitialAdClosedEvent += closed;
            IronSourceEvents.onInterstitialAdLoadFailedEvent += loadFailed;

            // load initial ad
            load();
        }

        bool IInterstitialProvider.available(){
            return IronSource.Agent.isInterstitialReady();
        }

        // Always show. Even when there is its no ready ( TODO: add 1s timeout )
        public void show(){
            show("DefaultInterstitial");
        }

        public void show(string placement){
            Debug.Log(string.Format("Interstitial(): show: {0}", placement));
            string showPlacement = "DefaultInterstitial";

            if (_lastRequestedPlacement != null && placement != null){
                showPlacement = _lastRequestedPlacement;
                _lastRequestedPlacement = null;
            }

            if (IronSource.Agent.isInterstitialReady())
                IronSource.Agent.showInterstitial(showPlacement);
            else{
                _lastRequestedPlacement = placement;
                IronSource.Agent.loadInterstitial();
            }
        }

        public void load(){
            Debug.Log("Interstitial(): load");
            IronSource.Agent.loadInterstitial();
        }

        private void ready(){
            Debug.Log("Interstitial(): ready");
            if (_lastRequestedPlacement != null)
                show(_lastRequestedPlacement);
            onReady();
        }

        private void opened(){
            onOpen();
        }

        private void closed(){
            IronSource.Agent.loadInterstitial();
            onClose();
        }

        private void loadFailed(IronSourceError obj){
            Debug.Log("Interstitial(): load failed" + obj.getDescription());
        }
    }
}