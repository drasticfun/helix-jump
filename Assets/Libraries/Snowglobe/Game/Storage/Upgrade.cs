﻿using Snowglobe.Core;

namespace Snowglobe.Game
{
    public class Upgrade : Storage<bool>
    {
        private string _baseKey;
        private int _maxUpgrades = 0;

        public Upgrade(PersistedFile file, string storageKey, string baseKey, int maxUpgrades) : base(file, storageKey)
        {
            _baseKey = baseKey;
            _maxUpgrades = maxUpgrades;
        }

        public int GetCurrent()
        {
            string keyFormat = _baseKey + "_{0}";

            int highest = -1;
            for (int currentUpgrade = 0; currentUpgrade < _maxUpgrades; currentUpgrade++)
            {
                string currentKey = string.Format(keyFormat, currentUpgrade);
                if (!this.Has(currentKey))
                    return highest;
                highest = currentUpgrade;
            }

            return highest;
        }

        public string GetCurrentKey()
        {
            return string.Format("{0}_{1}", _baseKey, GetCurrent());
        }

        public string GetNextKey()
        {
            return string.Format("{0}_{1}", _baseKey, GetCurrent() + 1);
        }

        public bool HasNext()
        {
            //TODO: TEST
            return GetCurrent() + 1 < _maxUpgrades;
        }

        public void Up()
        {
            if (HasNext())
            {
                this.Unlock(GetNextKey());
            }
        }
    }
}