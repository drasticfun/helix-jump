﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;
using Snowglobe.Virtuals;
using UnityEngine;

namespace Code.Virtuals
{
    [CreateAssetMenu(fileName = "Challenge", menuName = "Virtuals/Challenge", order = 2)]
    public abstract class SessionChallenge : Virtual
    {
        [SerializeField] private Virtual _reward;
        [SerializeField] private int _level;
        
        public Virtual Reward
        {
            get { return _reward; }
        }

        public int Level
        {
            get { return _level; }
        }

        public abstract Map GetSessionConfig();
    }
}