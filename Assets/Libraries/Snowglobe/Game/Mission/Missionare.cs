﻿using System;
using System.Collections.Generic;
using Snowglobe.Core;
using Snowglobe.Core.Storage;
using Snowglobe.Services;

namespace Code.Session
{
    public class Missionare : Service
    {
        private Analytics _analytics;
        
        public Act<GameMission> onMissionComplete = delegate(GameMission mission){ };
        private IList<GameMission> _missions;

        protected override void initialize(){
            _missions = App.Pool.Get<GameMission>();
            _analytics = App.Services.Get<Analytics>();
            Init();
        }

        private void Init()
        {
            ensureMissions();
            App.Stats.onUpdate += ensureMissions;   
        }

        // TODO: Fix Bug
        private void ensureMissions(){
            log(string.Format("ensureMissions:: Count: {0}", _missions.Count));
            
            List<GameMission> completeMissions = new List<GameMission>();
            List<string> unlockRewards = new List<string>();
            
            foreach (GameMission mission in _missions){
//                log(string.Format("Checking mission: {0}", mission.id));
                bool missionCompleted = App.CompletedMissions.Get(mission.Id);
//                log(string.Format("Completed: {0}", missionCompleted));
                if (!missionCompleted && checkCondition(mission)){
//                    log(string.Format("Mission condition met! Completiting"));
                    App.CompletedMissions.Set(mission.Id, true);

                    if (mission.reward != null){
//                        log(string.Format("Mission Reward: {0}", mission.reward.id));
                        string rewardId = mission.reward.Id;
                        bool unlocked = App.Unlocks.Get(rewardId);
                        if (!unlocked)
                            unlockRewards.Add(rewardId);
                    }
                    completeMissions.Add(mission);
                    _analytics.logEvent("mission_complete", "mission_id", mission.Id);
//                    log(string.Format("Mission condition met! --END--"));
                }
                else if (missionCompleted){
//                    log(string.Format("Mission already completed"));
                    // Ensure unlocked
                    string rewardId = mission.reward.Id;
                    bool unlocked = App.Unlocks.Get(rewardId);
                    if (!unlocked)
                        unlockRewards.Add(rewardId);
//                    log(string.Format("Mission already completed end"));
                }
            }

            foreach (string reward in unlockRewards)
            {
                App.Unlocks.Unlock(reward);
            }
            
            foreach (GameMission mission in completeMissions)
            {
                try
                {
                    onMissionComplete(mission);
                }
                catch (Exception ex)
                {
                    log(string.Format("Error triggering onMissionComplete event: \r\n{0}", ex.Message));
                }
            }
        }

        private bool checkCondition(GameMission mission){
//            log("checkCondition: start");
            StatCondition statCondition = mission.condition;
            
            if (statCondition == null || string.IsNullOrEmpty(statCondition.statKey))
                return false;
            
            string statKey = statCondition.statKey;
            float targetValue = statCondition.targetValue;

            float stat = App.Stats.Get(statKey);
            bool completed = false;
            switch (statCondition.condition){
                case StatCondition.ConditionType.MoreThan:
                    if (stat >= targetValue)
                        completed = true;
                    break;
                case StatCondition.ConditionType.LessThan:
                    if (stat <= targetValue)
                        completed = true;
                    break;
//                case StatCondition.ConditionType.Equals:
//                    if (stat >= targetValue - Mathf.Epsilon && stat <= targetValue + Mathf.Epsilon)
//                        completed = true;
//                    break;
            }

//            log("checkCondition: end");
            return completed;
        }
    }
}