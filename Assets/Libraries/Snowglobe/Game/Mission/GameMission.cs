﻿using System;
using Snowglobe.Virtuals;
using UnityEngine;

namespace Snowglobe.Services
{
    [CreateAssetMenu(fileName = "Mission", menuName = "Virtuals/GameMission", order = 2)]
    public class GameMission : Virtual
    {
        [SerializeField] private StatCondition _condition;
        [SerializeField] private Virtual _reward;
        
        public StatCondition condition{
            get{ return _condition; }
        }
        
        public Virtual reward{
            get{ return _reward; }
        }
    }

    [Serializable]
    public class StatCondition
    {
        public enum ConditionType
        {
            MoreThan,
            LessThan,
//            Equals
        }

        [SerializeField] private string _statKey;
        [SerializeField] private float _targetValue;
        [SerializeField] private ConditionType _condition;

        public string statKey{
            get{ return _statKey; }
        }

        public float targetValue{
            get{ return _targetValue; }
        }

        public ConditionType condition{
            get{ return _condition; }
        }
    }
}