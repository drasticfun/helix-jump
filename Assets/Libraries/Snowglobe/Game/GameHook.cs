﻿using Snowglobe.Core.Storage;

namespace Snowglobe.Game
{
    /// <summary>
    /// GameHook
    /// </summary>
    public abstract class GameHook
    {
        private Session _listenSession;

        protected Session Session
        {
            get { return _listenSession; }
        }

        public GameHook(Game game)
        {
            game.OnSessionInit += OnSessionInit;
        }

        private void OnSessionInit(Session session)
        {
            AddListeners(session);
            OnSessionReady();
        }

        private void AddListeners(Session session)
        {
            if (_listenSession != null && !_listenSession.Equals(session))
            {
                _listenSession.OnStart -= OnSessionStart;
                _listenSession.OnFinish -= OnSessionFinish;
                _listenSession.OnPause -= OnSessionPause;
            }
            else
            {
                _listenSession = session;
                _listenSession.OnStart += OnSessionStart;
                _listenSession.OnFinish += OnSessionFinish;
                _listenSession.OnPause += OnSessionPause;
            }
        }

        protected abstract void OnSessionReady();
        protected abstract void OnSessionStart();
        protected abstract void OnSessionFinish();
        protected abstract void OnSessionPause();
    }
}