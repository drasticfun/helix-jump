﻿using UnityEngine;

namespace Snowglobe.Virtuals
{
    /// <summary>
    /// Virtuals represent an entity
    /// on which the game uses to present a unique game item
    /// </summary>
    public class Virtual: ScriptableObject
    {
        [SerializeField]
        private string _id = "Unique-ID";
        
        [SerializeField]
        private string _display = "Name";
        
        [SerializeField]
        private string _description = "Description";
        
        [SerializeField]
        private Sprite _icon;

        public string Id
        {
            get { return GetId(); }
        }

        public Sprite Icon
        {
            get { return _icon; }
        }

        public string Display
        {
            get { return _display; }
        }

        public string Description
        {
            get { return _description; }
        }

        protected virtual string GetId()
        {
            return _id;
        }
    }
}