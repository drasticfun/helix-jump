﻿using System;
using Snowglobe.Core.Storage;
using System.Collections.Generic;
using UnityEngine;

namespace Snowglobe.Virtuals
{
    public class VirtualsPool
    {
        private IList<Virtual> _virtuals;

        public VirtualsPool(IList<Virtual> virtuals){
            _virtuals = virtuals;
        }

        public T Get<T>(string id) where T : Virtual{
            foreach (Virtual virt in _virtuals){
                if (virt.Id.Equals(id))
                    return (T) virt;
            }

            return null;
        }

        public List<T> Get<T>() where T : Virtual{
            List<T> list = new List<T>();
            foreach (Virtual virt in _virtuals){
                if (virt is T)
                    list.Add((T) virt);
            }

            return list;
        }
    }
}