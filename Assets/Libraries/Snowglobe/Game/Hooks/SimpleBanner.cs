﻿using Snowglobe.Core.Storage;
using Snowglobe.Game;
using Snowglobe.Services;
using UnityEngine;

namespace Snowglobe.GameHooks
{
    /// <summary>
    /// Presents a simple banner as long as:
    ///     The session is not started
    ///     NoAds is not purchased
    /// </summary>
    public class SimpleBanner : GameHook
    {
        private Ads _ads;

        public SimpleBanner(Game.Game game) : base(game){
            _ads = game.App.Services.Get<Ads>();
        }

        protected override void OnSessionReady(){
            _ads.Banner.show();
        }

        protected override void OnSessionStart(){
            _ads.Banner.show();
        }

        protected override void OnSessionFinish(){
//            _ads.banner.show();
        }

        protected override void OnSessionPause(){ }
    }
}