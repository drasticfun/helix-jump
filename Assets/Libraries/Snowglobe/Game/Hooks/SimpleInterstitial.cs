﻿using System.Collections.Generic;
using Snowglobe.Core.Storage;
using Snowglobe.Game;
using Snowglobe.Services;
using UnityEngine;

namespace Snowglobe.GameHooks
{
    // TODO: Condition Based Presentation

    /// <summary>
    /// Presents an interstitial after X sessions
    /// </summary>
    public class SimpleInterstitial : GameHook
    {
        public class Config
        {
            public const string SessionPattern = "interstitial_session_pattern";
            public const string MinPoiTime = "simple_interstitial_min_poi_time";
            public const string MaxPoiTime = "simple_interstitial_max_poi_time";
            public const string VideoPoiDelay = "simple_interstitial_video_poi_delay";
        }

        public class Placements
        {
            public const string IsSessionFinish = "is_session_finish";
        }

        private Game.Game _game;
        private Ads _ads;

//        private int _sessionInterval = 2;

        private float _minTime = 1 * 60;

        private float _maxTime = 10 * 60;
//        private float _videoDelay = 2 * 60;

        private float _startTime;

//        private int _currentSessionCount = 0;

        private InterstitialPattern _pattern;

        private bool _inSessionGrace = false;

        // TODO: Poi condition based on maintainted instance
//      private Map _instance = new Map();

        public SimpleInterstitial(Game.Game game) : base(game)
        {
            _game = game;
            _ads = game.App.Services.Get<Ads>();
            _ads.Interstitial.onOpen += onOpen;
            _ads.Video.onClose += onVideoClose;

            _startTime = Time.time;

            _pattern = new InterstitialPattern(_game.App.Config.get<string>(Config.SessionPattern, "1;2"));
            updateConfig();
        }

        private void updateConfig()
        {
//            Debug.Log(string.Format("-----------------------------------"));
//            Debug.Log("Simple Interstitial Update Config");
            _pattern.UpdatePattern(_game.App.Config.get<string>(Config.SessionPattern, "1;2"));
            _minTime = _game.App.Config.get(Config.MinPoiTime, 60);
            _maxTime = _game.App.Config.get(Config.MaxPoiTime, 180);
//            _videoDelay = _game.app.config.get(Config.VideoPoiDelay, 1 * 60f);
//            Debug.Log(string.Format("SimpleInterstitial:NewConfig sessionInterval/minTime/maxTime : {0}{1}{2}", _sessionInterval, _minTime, _maxTime));
//            Debug.Log(string.Format("-----------------------------------"));
        }

        private void onOpen()
        {
            _inSessionGrace = true;
            _pattern.ResetPings();
            _startTime = Time.time;
        }

        private void onVideoClose()
        {
            _inSessionGrace = true;
//            _startTime += _videoDelay;
        }

        public void poi()
        {
//            Debug.Log("Poi(): passedSess");
            updateConfig();
//            Debug.Log("SimpleInterstitial: poi()");
            float currentTime = Time.time;
//            float targetTime = currentTime;

            bool passedSessionCount = _pattern.ShouldShow();
            bool passedMinPoiTime = currentTime - _startTime >= _minTime;
            bool passedMaxPoiTime = currentTime - _startTime >= _maxTime;

            // ReSharper disable once PossibleLossOfFraction
//            bool loadBySession = (_sessionInterval - _currentSessionCount) / _sessionInterval >= 0.4f;
            // ReSharper disable once PossibleLossOfFraction
//            bool loadByTime = currentTime / targetTime >= 0.4f;

            // || loadByTime
//            if (loadBySession && !_ads.interstitial.available())
//                _ads.interstitial.load();

//            Debug.Log(string.Format("-----------------------------------"));
//            Debug.Log(string.Format("SimpleInterstitial: _sessionInterval/_minTime/_maxTime : {0}{1}{2}",_sessionInterval, _minTime, _maxTime));
//            Debug.Log(string.Format("SimpleInterstitial: sessionCount/passedMinPoiTime/passedMaxPoiTime : {0}{1}{2}", passedSessionCount , passedMinPoiTime , passedMaxPoiTime));
//            Debug.Log(string.Format("-----------------------------------"));

            // If user watched RV while in session dont show an IS
            if (_inSessionGrace)
            {
                _inSessionGrace = false;
                return;
            }

            if (passedMinPoiTime)
            {
                if (passedMaxPoiTime || passedSessionCount)
                {
                    _pattern.OnShow();
                    _ads.Interstitial.show(Placements.IsSessionFinish);
                }
            }
        }

        protected override void OnSessionReady()
        {
        }

        protected override void OnSessionStart()
        {
            _inSessionGrace = false;
        }

        protected override void OnSessionFinish()
        {
            if (!_inSessionGrace)
            {
                Debug.Log("SimpleInterstitial Pinging");
                _pattern.Ping();
            }

            poi();
        }

        protected override void OnSessionPause()
        {
        }
    }

    public class InterstitialPattern
    {
        private List<int> _frequencies;

        private int _currIndex = 0;
        private int _currPings = 0;

        public InterstitialPattern()
        {
            _frequencies = new List<int>();
            _frequencies.Add(2);
        }

        public InterstitialPattern(string pattern)
        {
            UpdatePattern(pattern);
        }

        public bool Ping()
        {
            int freq = _frequencies[_currIndex];
            _currPings++;
            bool shouldShow = _currPings >= freq;
            return shouldShow;
        }

        public bool ShouldShow()
        {
            return _currPings >= _frequencies[_currIndex];
        }

        public void ResetPings()
        {
            _currPings = 0;
        }

        public void OnShow()
        {
            _currPings = 0;
            _currIndex++;
            // Reset index if pass frequencies pattern count
            if (_currIndex >= _frequencies.Count)
                _currIndex = 0;
        }

        public void UpdatePattern(string pattern)
        {
            try
            {
                string[] patternArr = pattern.Split(';');
                _frequencies = new List<int>();
                for (int i = 0; i < patternArr.Length; i++)
                {
                    int freq = int.Parse(patternArr[i]);
                    _frequencies.Add(freq);
                }
            }
            catch
            {
                _frequencies = new List<int>();
                _frequencies.Add(2);
            }

            // Safety
            if (_frequencies == null || _frequencies.Count == 0)
            {
                _frequencies = new List<int>();
                _frequencies.Add(2);
            }

            while (_currIndex >= _frequencies.Count)
            {
                _currIndex--;
            }
        }
    }
}