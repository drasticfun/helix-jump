﻿using System.Collections.Generic;
using Facebook.Unity;
using Snowglobe.Core.Storage;
using Snowglobe.Game;
using UnityEngine;
using UnityEngine.Analytics;

namespace Snowglobe.GameHooks
{
    public class SimpleAnalytics: GameHook
    {
        private float _sessionStartTime;

        private Session _session;
        
        public SimpleAnalytics(Game.Game game) : base(game){
            
        }
        
        protected override void OnSessionReady(){
            
        }

        protected override void OnSessionStart(){
            _sessionStartTime = Time.time;
        }

        protected override void OnSessionFinish(){
            var data = new Dictionary<string, object>();
            
            data["session_duration"] = Time.time - _sessionStartTime;
            
            if (Session.Has("score"))
                data["session_score"] = Session.Get("score", -1);
            
            Analytics.CustomEvent("session_finish", data);
            
//            Firebase.Analytics.Parameter[] SessionFinishParams = {
//                new Firebase.Analytics.Parameter(
//                    "duration", Time.time - _sessionStartTime)
//            };
//            Firebase.Analytics.FirebaseAnalytics.LogEvent(
//                "session_finish",
//                SessionFinishParams);
            
//            FB.LogAppEvent(
//                "session_finish",
//                null, data
//            );
        }

        protected override void OnSessionPause(){
            
        }
    }
}