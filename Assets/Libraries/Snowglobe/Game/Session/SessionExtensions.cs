﻿namespace Snowglobe.Game
{
    public static class SessionExtensions
    {
        public static bool Has(this Session session, string key){
            return session.Instance.has(key);
        }
        
        public static void Put(this Session session, string key, object value){
            session.Instance.put(key, value);
        }

        public static T Get<T>(this Session session, string key, T def = default (T)){
            return session.Instance.get(key, def);
        }
    }
}