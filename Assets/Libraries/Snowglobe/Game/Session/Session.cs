﻿using Snowglobe.Core;
using Snowglobe.Core.Storage;

namespace Snowglobe.Game
{
    public abstract class Session : Object<App>
    {
        public class SessionStates
        {
            public const string Started = "started";
            public const string Paused = "paused";
            public const string Finished = "finished";
        }

        #region Session Lifecycle Events

        public abstract event Act OnReady;

        public event Act OnStart = delegate { };
        public event Act OnFinish = delegate { };
        public event Act OnResume = delegate { };
        public event Act OnPause = delegate { };

        #endregion

        #region Session Instance

        public Map Instance = new Map();
        public Map Config = new Map();

        #endregion

        #region Session Lifecycle

        public void StartSession()
        {
            StartSession(new Map());
        }

        public void StartSession(Map config)
        {
            Instance = new Map();
            Config = config;

            Instance.on(SessionStates.Started);
            OnStartSession();
            OnStart();
        }

        public void Pause()
        {
            if (!Instance.has(SessionStates.Paused))
            {
                Instance.on(SessionStates.Paused);
                OnPause();
            }
        }

        public void Resume()
        {
            if (Instance.has(SessionStates.Paused))
            {
                Instance.off(SessionStates.Paused);
                OnResume();
            }
            else
                log("Trying to resume while not paused");
        }

        public void Finish()
        {
            Instance.on(SessionStates.Finished);
            OnFinish();
            OnFinishSession();
        }
        
        protected abstract void OnStartSession();
        protected abstract void OnFinishSession();

        #endregion
    }
}