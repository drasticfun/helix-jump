﻿using Snowglobe.Core;

namespace Snowglobe.Services
{
    /// <summary>
    /// Tracks analytical events from the app:
    ///     stats
    ///     currencies
    ///     slots
    ///     toggles
    /// 
    /// </summary>
    public class ConfigurableTracker: Service
    {
        
    }
}