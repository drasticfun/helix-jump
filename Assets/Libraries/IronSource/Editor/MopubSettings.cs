#if UNITY_IPHONE 
using UnityEngine;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using System.IO;

namespace IronSource.Editor
{
	
	// Mopub is an empty adapter.
	// For every SDK update the following files should be replaced :
	// (Check documantation for any changes)
	// iOS:
		// libMoPubSDK.a
		// MoPub.bundle

	// Android: 
		// android-support-recyclerview-v7-23.1.1.jar
		// mopub-sdk-base.jar
		// mopub-sdk-rewardedvideo.jar
		// mopub-unity-plugins.jar
		// mopub-volley-1.1.0.jar
		// mopub-sdk-interstitial.jar
	
	public class MopubSettings : IAdapterSettings
	{

		private void RenameMRAIDSource (string buildPath)
		{
			// Unity will try to compile anything with the ".js" extension. Since mraid.js is not intended
			// for Unity, it'd break the build. So we store the file with a masked extension and after the
			// build rename it to the correct one.
			string buildTrimmedPath = buildPath.Replace("/Unity-iPhone.xcodeproj/project.pbxproj","");
			string[] maskedFiles = Directory.GetFiles (buildTrimmedPath, "*.prevent_unity_compilation", SearchOption.AllDirectories);
			foreach (string maskedFile in maskedFiles) {
				string unmaskedFile = maskedFile.Replace (".prevent_unity_compilation", "");
				File.Move(maskedFile, unmaskedFile);
			}
		}

		public void updateProject (BuildTarget buildTarget, string projectPath)
		{
			Debug.Log ("IronSource - Update project for Mopub");

			PBXProject project = new PBXProject ();
			project.ReadFromString (File.ReadAllText (projectPath));

			string targetId = project.TargetGuidByName (PBXProject.GetUnityTargetName ());

			// Required System Frameworks
			project.AddFrameworkToProject (targetId, "UIKit.framework", false);
			project.AddFrameworkToProject (targetId, "MediaPlayer.framework", false);
			project.AddFrameworkToProject (targetId, "SafariServices.framework", false);

			//Add MRAID framework to xcode
			RenameMRAIDSource (projectPath);

			File.WriteAllText (projectPath, project.WriteToString ());
		}

		public void updateProjectPlist (BuildTarget buildTarget, string plistPath)
		{
			Debug.Log ("IronSource - Update plist for Mopub");
		}
	}
}
#endif
