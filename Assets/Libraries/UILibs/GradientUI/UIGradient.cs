﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Effects/Gradient")]
public class UIGradient : BaseMeshEffect
{
    public Color m_color1 = Color.white;
    public Color m_color2 = Color.white;

    [Serializable]
    public class GradientColorDef
    {
        private Color _color = Color.white;
        private int _percent = 0;
    }

    [SerializeField] 
    private GradientColorDef[] _colorDefs = {new GradientColorDef()};

    [Range(-180f, 180f)] 
    public float m_angle = 0f;
    public bool m_ignoreRatio = true;

    public override void ModifyMesh(VertexHelper vh){
        if (enabled){
            Rect rect = graphic.rectTransform.rect;
            Vector2 dir = UIGradientUtils.RotationDir(m_angle);

            if (!m_ignoreRatio)
                dir = UIGradientUtils.CompensateAspectRatio(rect, dir);

            UIGradientUtils.Matrix2x3 localPositionMatrix = UIGradientUtils.LocalPositionMatrix(rect, dir);

            UIVertex vertex = default(UIVertex);
            for (int i = 0; i < vh.currentVertCount; i++){
                vh.PopulateUIVertex(ref vertex, i);
                Vector2 localPosition = localPositionMatrix * vertex.position;

                double percent = i / vh.currentVertCount * 100.00;
                // 1. Find out current location in percent
                vertex.color *= Color.Lerp(m_color2, m_color1, localPosition.y);
                Debug.Log(vertex.color);
                vh.SetUIVertex(vertex, i);
            }
        }
    }
}