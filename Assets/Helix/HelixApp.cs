﻿using System;
using System.Collections;
using System.Collections.Generic;
using Helix;
using Snowglobe.Core;
using Snowglobe.Services;
using UnityEngine;

public class HelixApp : App, Purchases.IProductsHandler {

	protected override void StartApp()
	{
		log("Helix Start");
	}

	public bool HandleProduct(string productId)
	{
		if (productId.Equals(HelixProducts.NoAds, StringComparison.OrdinalIgnoreCase))
		{
			Toggles.Set(Ads.Toggles.NoAds, true);
			return true;
		}
		
		return false;
	}
}
