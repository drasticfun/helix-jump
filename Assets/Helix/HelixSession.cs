﻿using Snowglobe.Core;
using Snowglobe.Game;
using UnityEngine;

namespace Helix
{
    public class HelixSession: Session
    {
        public override event Act OnReady = delegate {  };

        [SerializeField] private GameObject _gameRoot;
        
        protected override void initialize()
        {
            base.initialize();
            OnReady();
            GameManager.OnStart += () => { StartSession(); };
            GameManager.OnFinish += () => { Finish(); };
            
            _gameRoot.SetActive(true);
        }

        protected override void OnStartSession()
        {
            log("OnStartSession");
        }

        protected override void OnFinishSession()
        {
            log("OnFinishSession");
        }
    }
}