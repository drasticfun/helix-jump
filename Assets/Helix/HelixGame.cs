﻿using System;
using DG.Tweening;
using Snowglobe.Core;
using Snowglobe.Game;
using Snowglobe.Services;
using UnityEngine;
using UnityEngine.UI;

namespace Helix
{
    public class HelixGame : Game
    {
        public override event Act<Session> OnSessionInit = delegate(Session session) { };
        
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private Session _session;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private Button _soundButton;
        [SerializeField] private Button _ballsButton;
        [SerializeField] private Button _leaderboardsButton;
        [SerializeField] private Button _noadsButton;
        [SerializeField] private Button _reviveButton;
        [SerializeField] private Button _restorePurchasesButton;
        
        [SerializeField] private Text _comingSoonText;

        [SerializeField] private Sprite _soundOnSprite;
        [SerializeField] private Sprite _soundOffSprite;
        
        private Purchases _purchases;
        private Ads _ads;
        private Sound _sound;
        private Leaderboards _leaderboards;

        public Sound Sound
        {
            get { return _sound; }
        }

        protected override void preInitialize()
        {
            base.preInitialize();
            _sound = getService<Sound>();
            _session.OnReady += () => OnSessionInit(_session);
        }

        protected override void initialize()
        {
            base.initialize();
            _purchases = getService<Purchases>();
            _ads = getService<Ads>();
            _leaderboards = getService<Leaderboards>();
            _reviveButton.onClick.AddListener(Revive);
            _soundButton.onClick.AddListener(ToggleSound);
            _noadsButton.onClick.AddListener(BuyNoAds);
            _restorePurchasesButton.onClick.AddListener(RestorePurchases);
            _leaderboardsButton.onClick.AddListener(ShowLeaderboards);
            _ballsButton.onClick.AddListener(ShowBalls);
            UpdateSoundButton();
        }

        private void ShowBalls()
        {
            _comingSoonText.DOFade(1f, 0.25f).onComplete += ()=> _comingSoonText.DOFade(0, 0.25f).SetDelay(1.0f);
        }

        private void Revive()
        {
            _gameManager.RequestRevive();
            _ads.Video.show(HelixPlacements.VideoRevive);
        }

        private void ToggleSound()
        {
            bool enabled = App.Prefs.Get(SoundService.Enabled);
            App.Prefs.Set(SoundService.Enabled, !enabled);
            UpdateSoundButton();
        }

        protected override void onActive()
        {
            _ads.Video.onReward += OnVideoReward;
        }

        protected override void onDeactive()
        {
            _ads.Video.onReward -= OnVideoReward;
        }

        private void UpdateSoundButton()
        {
            bool enabled = App.Prefs.Get(SoundService.Enabled);
            if (enabled)
                _soundButton.image.sprite = _soundOnSprite;
            else
                _soundButton.image.sprite = _soundOffSprite;
        }

        private void BuyNoAds()
        {
            _purchases.Buy(HelixProducts.NoAds);
        }

        private void RestorePurchases()
        {
            _purchases.Restore();
        }

        private void ShowLeaderboards()
        {
            _leaderboards.Show();
        }
        
        private void OnVideoReward(string placement)
        {
            if (placement.Equals(HelixPlacements.VideoRevive, StringComparison.OrdinalIgnoreCase))
            {
                _gameManager.ReviveGame();
            }
        }
    }
}